﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSJL.Framework.Utils;
using Qiniu.IO;
using Qiniu.RPC;
using SJL.Plugin.Third;

namespace SJL.Plugin.Third
{
    public class QiniuImageMng
    {

        #region 上传图片
        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="filePath">图片路径</param>
        /// <returns></returns>
        public static QiniuUploadResponse UploadImage(string filePath)
        {
            QiniuHelper.Init();
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException("filePath不能为空");
            }

            if (!File.Exists(filePath))
            {
                throw new ArgumentNullException("找不到filePath对应的文件");
            }

            string fileName = SelfUtil.GetGUID();
            PutRet ret = QiniuHelper.PutImage(fileName, filePath);

            var response = new QiniuUploadResponse();
            if (ret.OK)
            {
                response.IsOK = true;
                response.FileName = ret.key;
                return response;
            }
            response.IsOK = false;
            response.Msg = ret.Exception.Message;
            response.FileName = fileName;
            return response;
        }

        public static QiniuUploadResponse UploadImageNew(string filePath)
        {
            QiniuHelper.Init();
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException("filePath不能为空");
            }
            if (!File.Exists(filePath))
            {
                throw new ArgumentNullException("找不到filePath对应的文件");
            }
            string fileName = Path.GetFileName(filePath);
            PutRet ret = QiniuHelper.PutImage(fileName, filePath);

            var response = new QiniuUploadResponse();
            if (ret.OK)
            {
                response.IsOK = true;
                response.FileName = ret.key;
                return response;
            }
            response.IsOK = false;
            response.Msg = ret.Exception.Message;
            response.FileName = fileName;
            return response;
        }
        #endregion

        #region 删除图片
        /// <summary>
        /// 删除图片
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public static QiniuDeleteResponse DeleteImage(string fileName)
        {
            QiniuHelper.Init();
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentNullException("fileName不能为空");
            }

            CallRet ret = QiniuHelper.Delete(QiniuHelper.bucket, fileName);

            var response = new QiniuDeleteResponse();
            if (ret.OK)
            {
                response.IsOK = true;
                return response;
            }

            response.IsOK = false;
            response.Msg = ret.Exception.Message;
            return response;
        }
        #endregion

        #region 获取图片
        /// <summary>
        /// 获取图片
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public static string GetImage(string fileName, int width=100,int height=100)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return "";
            }

            string imageParam = string.Format("?imageView2/1/w/{0}/h/{1}/q/100", width,height);///q/30

            return QiniuHelper.ImageUrlPredix + fileName + imageParam;
        }

        public static string GetOriginalImage(string fileName)
        {
            return QiniuHelper.ImageUrlPredix + fileName;
        }
        /// <summary>
        /// 获取低质量原图
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetLowQualityImage(string fileName)
        {
            return QiniuHelper.ImageUrlPredix + fileName+ "?imageView2/1/q/30";
        }
        
        #endregion
    }
}
