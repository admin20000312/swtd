﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class BarCodeHelper
    {
        private static readonly object locks = new object();
        private static BarCodeHelper manage;
        public static BarCodeHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new BarCodeHelper());
            }
        }
        public Action<string> CallBackStr = null;
        private SerialPortHelp sp = null;
        private CachedBuffer cache = new CachedBuffer();
        public int CardType { get; set; } = 0; //0表示  新大陆    1表示基恩士    2新大陆手持 
        public CommonResult Start(string portName, int baudRate, Action<string> action, int cardtype = 0)
        {
            try
            {
                cache.Clear();

                CardType = cardtype;
                CallBackStr = action;
                if (sp == null)
                {
                    sp = new SerialPortHelp();
                    sp.CallBackAction = DataReceived;
                    if (cardtype == 0 || cardtype == 2)   //新大陆
                    {
                        return sp.Start(portName, baudRate);
                    }
                    else
                    {
                        return sp.Start(portName, baudRate, Parity.Even);
                    }
                }
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
                return new CommonResult() { result = false };
            }
        }
        public static DateTime LastTime = DateTime.Now;
        public void DataReceived(byte[] buffer)
        {
            try
            {
                cache.Clear();
                cache.Write(buffer, 0, buffer.Length);
                var temp = cache.ReadDataWithBarCode(CardType);
                if (temp == null)
                {
                    return;
                }
                if (CallBackStr != null)
                {
                    var str = "";
                    if (CardType == 0) //新大陆
                    {
                        var bytes = new byte[temp.Length - 2];
                        Array.Copy(temp, 0, bytes, 0, bytes.Length);
                        str = Encoding.UTF8.GetString(bytes).Replace("\r", "").Replace("\n", "");
                    }
                    else if (CardType == 1 || CardType == 2)  //基恩士     手持新大陆
                    {
                        var bytes = new byte[temp.Length - 1];
                        Array.Copy(temp, 0, bytes, 0, bytes.Length);

                        str = Encoding.UTF8.GetString(bytes).Replace("\r", "").Replace("\n", "");
                    }
                    if (DateTime.Now.AddSeconds(-2) < LastTime)
                    {
                        return;
                    }
                    LastTime = DateTime.Now;

                    TextLogUtil.Info("扫码枪内容：" + str);

                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        Task.Run(() =>
                        {
                            CallBackStr(str);
                        });
                    }
                    return;
                }
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
        public void Stop()
        {
            try
            {
                CallBackStr = null;
                sp?.Close();
                sp = null;
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

    }
}
