﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DJ.Clients;
using DJ.Dialog;
using DJ.Third;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NPOI.SS.Formula.Functions;
using NSJL.Biz.Background.Client;
using NSJL.Biz.Background.Quartzs;
using NSJL.DAL.DataModel.Entities;
using NSJL.DAL.DataModel.Models;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using NSJL.Services.Console;
using BoxInfo = NSJL.DAL.DataModel.Models.BoxInfo;

namespace DJ
{
    /// <summary>
    /// Client.xaml 的交互逻辑
    /// </summary>
    public partial class Client : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        string Cabinet { get; set; }
        public Client()
        {
            InitializeComponent();
            ComWindow.Start(this);
            Cabinet = ini.readKey("BoxConfig", "cabinetName");
            TextLogUtil.Info(Cabinet+"柜启动成功");
            #region 锁板串口开启,只启动一次
            Task.Run(() =>
            {
                try
                {
                    
                    var ini = new IniFile("Config/Config.ini");
                    var portName = ini.readKey("BoxConfig", "portName");
                    var baudRate = ini.readKey("BoxConfig", "baudRate");
                    var result = LockManager.GetInstance().Start(portName, baudRate.ToInt32());
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }
                   
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                }
            });
            #endregion

            #region 连接读写器
            Task.Run(() =>
            {
                try
                {
                    //连接读写器
                    var port = ini.readKey("InventoryConfig", "port").Trim().ToInt32();
                    var res = RFIDHelper.GetInstance().ConnectNetWork(port);
                    if (!res.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(res.message);
                            return;
                        });
                    }
                }
                catch (Exception e)
                {
                    TextLogUtil.Info("读写器连接异常：" + e.Message);
                    Dispatcher.Invoke(() =>
                    {
                        MessageDialog.ShowDialog("读写器连接异常：" + e.Message);
                    });

                }
            });
            #endregion
        }

        private UserInfo user = null;
        //放样/加急（存，Tag 0:放样、1:加急）
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                InputSampleNum isn = new InputSampleNum();
                if (isn.ShowDialog() != true)
                {
                    return;
                }
                int num = isn.Num;//样品袋数量
                ScannerSample ss = new ScannerSample(num);
                if (ss.ShowDialog() != true)
                {
                    return;
                }
                List<string> sampList = ss.sampList;//扫描的所有样品袋数据
                int count = sampList.Count;
                if (num != count)
                {
                    MessageDialog.ShowDialog("扫描样品袋的数量与输入的数量不一致");
                    return; 
                }
                ScannerSampleBox ssb = new ScannerSampleBox();
                if (ssb.ShowDialog() != true)
                {
                    return;
                }
                string text = ssb.text;
                var result = InterBiz.GetBoxByCab(Cabinet);
                if (!result.result)
                {
                    MessageDialog.ShowDialog("箱门获取失败,原因："+result.message);
                    return;
                }
                var freeBox = result.data.Where(p=>p.State == "空闲" && p.IsLock != 1).FirstOrDefault();
                if(freeBox == null)
                {
                    MessageDialog.ShowDialog("开箱失败，没有空闲箱门");
                    return;
                }
                LockManager.GetInstance().OpenBox(freeBox.BoxNum);
                VoiceHelper.GetInstance().Start(freeBox.BoxNum + "号箱已开启，操作完成后请随手关门");
                SuccessTip st = new SuccessTip(freeBox.BoxNum);//会循环检测箱门是否关闭
                st.mess.Content = freeBox.BoxNum + "号箱门已打开，操作完成后请随手关门";
                if (st.ShowDialog() != true)
                {
                    FailTip fail = new FailTip(freeBox.BoxNum);
                    if (fail.ShowDialog() != true)
                    {
                        TextLogUtil.Info(freeBox.BoxNum + "号箱门，借出后关门异常");
                        return;
                    }
                }
                //开始盘点
                //箱门关闭后，开始盘点
                Inventory ins = new Inventory(freeBox.BoxNum);
                if (ins.ShowDialog() != true)
                {
                    return;
                }
                var list = ins.InvList;
                if(list.Count <= 0)
                {
                    MessageDialog.ShowDialog("未盘点到样品箱");
                    return;
                }
                //修改箱门状态
                Button btn = (Button)sender;
                var tag = btn.Tag.ToString();
                string state = tag == "0" ? "在库" : "加急";
                Comm com = new Comm() { BoxNum = freeBox.BoxNum, CabName = Cabinet, SampleBoxEpc = list[0], SampleNum = count, State = state };
                var result1 = InterBiz.EditBoxAndSampleBox(com);
                if(!result1.result)
                {
                    MessageDialog.ShowDialog(result1.message);
                    return;
                }
                AccessLog accessLog = new AccessLog() { Id = Guid.NewGuid().ToString("N"), BoxNum = freeBox.BoxNum, SampleBoxEpc = list[0],CabNum = Cabinet };
                var res = InterBiz.AddAccessLog(0, accessLog);
                if (!res.result)
                {
                    MessageDialog.ShowDialog(res.message);
                    return;
                }
                MessageDialog.ShowDialog("放样成功");
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }
        //送样
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var result = InterBiz.GetBoxByCab(Cabinet);
            if(!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            List<BoxInfo> boxList = result.data.Where(p => p.State != "空闲").OrderBy(p=>p.BoxNum).ToList();
            ChooseSendBox csb = new ChooseSendBox(boxList);
            if (csb.ShowDialog() != true)
            {
                return;
            }
            BoxClient selectBox = csb.selectBox;
            int boxNum = selectBox.BoxNum;
            var box = InterBiz.GetBoxByCab(Cabinet).data.Where(p=>p.BoxNum == boxNum).FirstOrDefault();
            if (box.IsLock == 1)
            {
                DJMessage djm = new DJMessage();
                djm.tip.Content = "送样提醒";
                djm.msg.Content = "当前箱门已上锁，开箱请联系管理员!";
                djm.ShowDialog();
                return;
            }
        ReOpen:
            LockManager.GetInstance().OpenBox(boxNum);
            VoiceHelper.GetInstance().Start(boxNum + "号箱已开启，操作完成后请随手关门");
            SuccessTip st = new SuccessTip(boxNum);
            st.mess.Content = boxNum + "号箱门已打开，操作完成后请随手关门";
            if (st.ShowDialog() != true)
            {
                FailTip fail = new FailTip(boxNum);
                if (fail.ShowDialog() != true)
                {
                    TextLogUtil.Info(boxNum + "号箱门，借出后关门异常");
                    return;
                }
            }
            //开始盘点
            //箱门关闭后，开始盘点
            Inventory ins = new Inventory(boxNum);
            if (ins.ShowDialog() != true)
            {
                return;
            }
            var list = ins.InvList;
            if (list !=null && list.Count > 0)
            {
                MessageDialog.ShowDialog("样品箱未取出");
                goto ReOpen;
            }
            BoxInfo boxInfo = boxList.Where(p => p.BoxNum == boxNum).FirstOrDefault();
            Comm com = new Comm() { BoxNum = boxNum, CabName = Cabinet, SampleBoxEpc = boxInfo.SampleBoxEpc, State = "送检中" };
            var result1 = InterBiz.EditBoxAndSampleBox(com);
            if (!result1.result)
            {
                MessageDialog.ShowDialog(result1.message);
                return;
            }
            AccessLog accessLog = new AccessLog() { Id = Guid.NewGuid().ToString("N"), BoxNum = boxNum, SampleBoxEpc = boxInfo.SampleBoxEpc, CabNum = Cabinet };
            var res = InterBiz.AddAccessLog(1, accessLog);
            if (!res.result)
            {
                MessageDialog.ShowDialog(res.message);
                return;
            }
            MessageDialog.ShowDialog("送样取出成功");
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Login login = new Login();
                login.ShowDialog();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
