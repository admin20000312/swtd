﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// InputSampleNum.xaml 的交互逻辑
    /// </summary>
    public partial class InputSampleNum : Window
    {
        public InputSampleNum()
        {
            InitializeComponent();
            ComWindow.Start(this, Label1);
            VoiceHelper.GetInstance().Start("请输入放到样品箱中的样品袋数量！");
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public int Num { get; set; } = 0;

        //确认
        private void Button_Ok(object sender, RoutedEventArgs e)
        {
            string num = tbNum.Text;
            if (!int.TryParse(num,out int n))
            {
                MessageDialog.ShowDialog("请输入正确的样品袋数量");
                return;
            }
            Num = Convert.ToInt32(num);
            this.DialogResult = true;
        }
    }
}
