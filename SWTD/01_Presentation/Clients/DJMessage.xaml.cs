﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Clients
{
    /// <summary>
    /// DJMessage.xaml 的交互逻辑
    /// </summary>
    public partial class DJMessage : Window
    {
        public DJMessage()
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);
            VoiceHelper.GetInstance().Start("当前箱门已上锁，开箱请联系管理员!");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
