﻿using DJ.Dialog;
using DJ.UserControls;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Models;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// ChooseSendBox.xaml 的交互逻辑
    /// </summary>
    public partial class ChooseSendBox : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        string Cabinet { get; set; }
        List<BoxInfo> boxes {  get; set; }//所有非空闲的箱门
        public ChooseSendBox(List<BoxInfo> list)
        {
            InitializeComponent();
            ComWindow.Start(this, Label1);
            boxes = list;
            Cabinet = ini.readKey("BoxConfig", "cabinetName");
            if (boxes != null && boxes.Count > 0)
            {
                Task.Run(() =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        Init(list);
                    });
                });
            }
        }



        public void Init(List<BoxInfo> data)
        {
            WrapPanel1.Children.Clear();
            foreach (var item in data)
            {
                BoxClient btn = new BoxClient();
                btn.Width = 150;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Text = "箱门号:"+ item.BoxNum;
                btn.BoxNum = item.BoxNum;
                btn.Label2.Text = "状态:" + item.State;
                var res = InterBiz.GetAllSampleBox();
                if (!res.result)
                {
                    MessageDialog.ShowDialog(res.message);
                    return;
                }
                var sampleBoxInfo = res.data.Where(p => p.SampleBoxEpc == item.SampleBoxEpc).FirstOrDefault();
                if (sampleBoxInfo != null)
                {
                    btn.Label3.Content = "样品数量:"+ (sampleBoxInfo.SampleNum ?? 0);
                }
                else
                {
                    btn.Label3.Content = "样品袋数量：0";
                }
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }

        public BoxClient selectBox {  get; set; }

        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxClient)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                var filepath = "pack://application:,,,/ClientImage0/Group boxBJ@2x.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                selectBox = null;
            }
            else
            {
                var children = WrapPanel1.Children;
                foreach (var control in children)
                {
                    if(control is BoxClient bc)
                    {
                        var path = "pack://application:,,,/ClientImage0/Group boxBJ@2x.png";
                        bc.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(path)));
                        bc.IsCheck = false;
                    }
                }
                u.IsCheck = true;
                var filepath = "pack://application:,,,/ClientImage0/Group boxBJ1@2x.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                selectBox = u;
            }
        }




        /// <summary>
        /// 返回
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 下一步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Ok(object sender, RoutedEventArgs e)
        {
            if (selectBox == null)
            {
                MessageDialog.ShowDialog("请先选择箱门");
                return;
            }
            this.DialogResult = true;
        }


        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
