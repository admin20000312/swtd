﻿using DJ.ZEF.ThirdParty;
using NPOI.SS.Formula.Functions;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// ScannerSampleBox.xaml 的交互逻辑
    /// </summary>
    public partial class ScannerSampleBox : Window
    {
        public string text {  get; set; }
        public ScannerSampleBox()
        {
            InitializeComponent();
            ComWindow.Start(this, Label1);
            VoiceHelper.GetInstance().Start("请扫描样品箱上的二维码");
            var ini = new IniFile("Config/Config.ini");
            var portName = ini.readKey("BarCodeConfig", "portName");
            var baudRate = ini.readKey("BarCodeConfig", "baudRate").Trim().ToInt32();
            BarCodeHelper.GetInstance().Start(portName, baudRate, (a) =>
            {
                Dispatcher.Invoke(() =>
                {
                    TextLogUtil.Info("扫描到的二维码：" + a);
                    if (!string.IsNullOrWhiteSpace(a))
                    {
                        text = a;
                        DialogResult = true;
                    }
                });
            }, 0);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            BarCodeHelper.GetInstance().Stop();
        }
    }
}
