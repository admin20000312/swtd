﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Clients
{
    /// <summary>
    /// SuccessTip.xaml 的交互逻辑
    /// </summary>
    public partial class SuccessTip : Window
    {
        DispatcherTimer timer= new DispatcherTimer();

        public SuccessTip(int BoxNum)
        {
            InitializeComponent();
            ComWindow.Start(this);

            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                var miao = Label1.Tag.ToInt32() - 1;
                Label1.Tag = miao;

                if (miao <= 85)
                {
                    //检测箱门
                    LockManager.GetInstance().CheckBox(BoxNum, (isclose) =>
                    {
                        if (isclose == false)
                        {
                            Dispatcher.Invoke(() =>
                            {
                                Close();
                            });
                        }
                    });
                }

                if (miao >= 0 && miao <= 30 && (miao % 3==0))
                {
                    //开启警报声音
                    VoiceHelper.GetInstance().Start("请关闭箱门");
                }
                if (miao <= 0)
                {
                    this.Close();
                    return;
                }
                Label1.Content = "倒计时：" + Label1.Tag + "秒";
            });
            timer.IsEnabled = true;
            timer.Start();
        }
        //public int BoxNum { get; set; }
        private void Window_Closed(object sender, EventArgs e)
        {
            timer.Stop();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }

        
    }
}
