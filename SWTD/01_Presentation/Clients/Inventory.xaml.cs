﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;

namespace DJ.Clients
{
    /// <summary>
    /// Inventory.xaml 的交互逻辑
    /// </summary>
    public partial class Inventory : Window
    {
        //public List<RFIDHelper.RFIDInfo> list=new List<RFIDHelper.RFIDInfo>();
        public Inventory(int boxnum)
        {
            InitializeComponent();
            ComWindow.Start(this);
            BoxNum = boxnum;
        }
        public int BoxNum { get; set; }
        public List<string> InvList = new List<string>();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Task.Run(() =>
            {
                try
                {
                    List<RFIDHelper.RFIDInfo> RFIDInfoList = RFIDHelper.GetInstance().StartOnceReadByAnt(BoxNum, 5);
                    foreach (var item in RFIDInfoList)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            string labelId = item.LabelId;
                            InvList.Add(labelId);
                        });
                    }
                    Dispatcher.Invoke(() =>
                    {
                        DialogResult = true;
                        Close();
                    });
                }
                catch (Exception ex)
                {
                    Dispatcher.Invoke(() =>
                    {
                        MessageDialog.ShowDialog(ex.Message);
                        Close();
                    });
                }
            });

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            //RFIDHelper.GetInstance().Close();
        }

        
    }
}
