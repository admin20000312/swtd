﻿using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// SannerSample.xaml 的交互逻辑
    /// </summary>
    public partial class ScannerSample : Window
    {
        int Num {  get; set; }
        public List<string> sampList = new List<string>();
        public ScannerSample(int num)
        {
            InitializeComponent();
            Label1.Content = (num * 5) +"s";
            Label1.Tag = (num * 5) > 30 ?(num * 5):30;
            ComWindow.Start(this,Label1);
            VoiceHelper.GetInstance().Start("请依次扫描所有样品袋上的二维码");
            Num = num;
            var ini = new IniFile("Config/Config.ini");
            var portName = ini.readKey("BarCodeConfig", "portName");
            var baudRate = ini.readKey("BarCodeConfig", "baudRate").Trim().ToInt32();
            BarCodeHelper.GetInstance().Start(portName, baudRate, (a) =>
            {
                Dispatcher.Invoke(() =>
                {
                    TextLogUtil.Info("扫描到的二维码：" + a);
                    if (!string.IsNullOrWhiteSpace(a) && !sampList.Contains(a))
                    {
                        sampList.Add(a);
                        if (sampList.Count == Num)
                        {
                            this.DialogResult = true;
                        }
                    }
                    else
                    {
                        VoiceHelper.GetInstance().Start("样品袋已扫描过");
                    }

                });
            }, 0);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            BarCodeHelper.GetInstance().Stop();
        }
    }
}
