﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Pages
{
    /// <summary>
    /// EditPwdPage.xaml 的交互逻辑
    /// </summary>
    public partial class EditPwdPage : Page
    {
        public EditPwdPage()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string pwdOld = PasswordBox1.Password;
            string pwdNew1 = PasswordBox2.Password;
            string pwdNew2 = PasswordBox3.Password;
            if (string.IsNullOrEmpty(pwdOld))
            {
                MessageBox.Show("老密码不能为空！");
                return;
            }
            if ((string.IsNullOrEmpty(pwdNew1)||string.IsNullOrEmpty(pwdNew2)) && pwdNew1 != pwdNew2)
            {
                MessageBox.Show("输入的两次新密码不一致");
                return;
            }

            var ini = new IniFile("Config/login.ini");
            string pwd = ini.readKey("Login", "password");
            if (pwdOld != pwd)
            {
                MessageDialog.ShowDialog("旧密码不正确");
                return;
            }
            ini.writeKey("Login", "password", pwdNew2);
            MessageDialog.ShowDialog("修改成功");
            PasswordBox1.Password = null;
            PasswordBox2.Password = null;
            PasswordBox3.Password = null;
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}
