﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Models;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Pages
{
    /// <summary>
    /// BoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class BoxPage : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        string Cabinet {  get; set; }
        public BoxPage()
        {
            InitializeComponent();
            Cabinet = ini.readKey("BoxConfig", "cabinetName");
        }
        List<BoxInfo> data { get; set; }
        List<BoxControl> listBox = new List<BoxControl>();
        public void Init()
        {
            listBox.Clear();
            var result = InterBiz.GetBoxByCab(Cabinet);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            WrapPanel1.Children.Clear();
            data = result.data.OrderBy(p=>p.BoxNum).ToList();
            foreach (var item in data)
            {
                BoxControl btn = new BoxControl();
                btn.Width = 150;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;

                if (item.IsLock == 1)
                {
                    btn.Label3.Content = "已锁定";
                    btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label3.Content = "未锁定";
                }
                btn.Label2.Text = "箱柜号：" + item.CabNum.ToString();
                btn.Label4.Text = "状态：" + item.State;
                var res = InterBiz.GetAllSampleBox();
                if (!res.result)
                {
                    MessageDialog.ShowDialog(res.message);
                    return;
                }
                var sampleBoxInfo = res.data.Where(p => p.SampleBoxEpc == item.SampleBoxEpc).FirstOrDefault();
                if (sampleBoxInfo != null)
                {
                    btn.Label5.Content = "样品袋数量：" + (sampleBoxInfo.SampleNum ?? 0);
                }
                else
                {
                    btn.Label5.Content = "样品袋数量：0";
                }
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }

        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                var filepath = "pack://application:,,,/Image/a未选中.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                listBox.Remove(u);
            }
            else
            {
                u.IsCheck = true;

                var filepath = "pack://application:,,,/Image/a选中.png";

                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                listBox.Add(u);
            }
        }


        //开箱
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                string str = string.Join(",", list.ToArray());
                Task.Run(() =>
                {
                    foreach (var item in list)
                    {
                        LockManager.GetInstance().OpenBox(item);
                        Thread.Sleep(600);
                        AccessLog al = new AccessLog() { Id = Guid.NewGuid().ToString("N"), BoxNum = item, CabNum = Cabinet };
                        var result = InterBiz.AddAccessLog(0,al);
                        if (!result.result)
                        {
                            TextLogUtil.Info(str + "号箱子日志上传失败，错误消息：" + result.message);
                        }
                    }
                });
                var biz=new CommBiz();
                var tip = str + "号箱门开箱成功";
                VoiceHelper.GetInstance().Start(tip);
                MessageDialog.ShowDialog(str + "开箱成功");
                
                
            }
        }
        //全开
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            MessageDialog mes=new MessageDialog();
            mes.TextBlock1.Text = "是否确定操作";
            if (mes.ShowDialog() != true)
            {
                return;
            }
            Task.Run(() =>
            {
                foreach (var item in data)
                {
                    LockManager.GetInstance().OpenBox(item.BoxNum);
                    Thread.Sleep(600);
                    AccessLog al = new AccessLog() { Id = Guid.NewGuid().ToString("N"), BoxNum = Convert.ToInt32(item.BoxNum), CabNum = Cabinet };
                    var result = InterBiz.AddAccessLog(0, al);
                    if (!result.result)
                    {
                        TextLogUtil.Info(item.BoxNum + "号箱子开箱日志上传失败，错误消息：" + result.message);
                    }
                }
            });
            var tip = "全开箱门操作成功";
            VoiceHelper.GetInstance().Start(tip);
            MessageDialog.ShowDialog("全开箱门操作成功");
        }


        //清箱
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                foreach (BoxControl item in WrapPanel1.Children)
                {
                    if (list.Contains(item.BoxNum))
                    {
                        item.Label2.Text = "箱柜号:" + Cabinet;
                        item.Label4.Text = "状态：空闲";
                        item.Label5.Content = "样品袋数量：0";
                        BoxInfo boxInfo = data.Where(p => p.CabNum == Cabinet && p.BoxNum == item.BoxNum).FirstOrDefault();
                        boxInfo.State = "空闲";
                        boxInfo.SampleBoxEpc = null;
                        boxInfo.IsLock = 0;
                        boxInfo.UpdateTime =null;
                        boxInfo.CreateTime =null;
                        var res = InterBiz.EditBox(boxInfo);
                        if (!res.result)
                        {
                            TextLogUtil.Info("清箱异常："+res.message);
                        }
                    }
                }
                string str = string.Join(",", list.ToArray());
                var tip = str + "号箱门清箱成功";
                VoiceHelper.GetInstance().Start(tip);
                MessageDialog.ShowDialog(str + "清箱成功");
            }
        }
        //全清
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            MessageDialog mes = new MessageDialog();
            mes.TextBlock1.Text = "是否确定操作";
            if (mes.ShowDialog() != true)
            {
                return;
            }
            foreach (BoxControl item in WrapPanel1.Children)
            {
                item.Label2.Text = "箱柜号:"+Cabinet;
                item.Label4.Text = "状态：空闲";
                item.Label5.Content = "样品袋数量：0";
                BoxInfo boxInfo = data.Where(p => p.CabNum == Cabinet && p.BoxNum == item.BoxNum).FirstOrDefault();
                boxInfo.State = "空闲";
                boxInfo.SampleBoxEpc = null;
                boxInfo.IsLock = 0;
                boxInfo.UpdateTime = null;
                boxInfo.CreateTime = null;
                var res = InterBiz.EditBox(boxInfo);
                if (!res.result)
                {
                    TextLogUtil.Info("全清异常：" + res.message);
                }
            }

            var tip ="全部清箱成功";
            VoiceHelper.GetInstance().Start(tip);
            MessageDialog.ShowDialog("全部清箱成功");
        }
        //锁箱
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    BoxInfo boxInfo = data.Where(p => p.CabNum == Cabinet && p.BoxNum == item).FirstOrDefault();
                    boxInfo.IsLock = 1;
                    boxInfo.UpdateTime = null;
                    boxInfo.CreateTime = null;
                    var res = InterBiz.EditBox(boxInfo);
                    if (!res.result)
                    {
                        TextLogUtil.Info("锁箱异常：" + res.message);
                    }
                }
                string str = string.Join(",", list.ToArray());
                var tip = str + "号箱门锁箱成功";
                VoiceHelper.GetInstance().Start(tip);
                MessageDialog.ShowDialog(str + "锁箱成功");
                Init();
            }
        }
        //解锁
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    BoxInfo boxInfo = data.Where(p => p.CabNum == Cabinet && p.BoxNum == item).FirstOrDefault();
                    boxInfo.IsLock = 0;
                    boxInfo.UpdateTime = null;
                    boxInfo.CreateTime = null;
                    var res = InterBiz.EditBox(boxInfo);
                    if (!res.result)
                    {
                        TextLogUtil.Info("解箱异常：" + res.message);
                    }
                }
                string str = string.Join(",", list.ToArray());
                var tip = str + "号箱门解箱成功";
                VoiceHelper.GetInstance().Start(tip);
                MessageDialog.ShowDialog(str + "解箱成功");

                Init();
            }
        }
        private List<int> GetSelectBox()
        {
            List<int> list = new List<int>();
            foreach (BoxControl item in WrapPanel1.Children)
            {
                if (item.IsCheck)
                {
                    list.Add(item.BoxNum);
                }
            }
            return list;
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Cabinet = ini.readKey("BoxConfig", "cabinetName");
            Init();
        }
        //全选
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button click = (Button) sender;
            var name = (string)click.Content;


            foreach (BoxControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name!="全选")
                {
                    click.Content = "全选";
                    
                    u.IsCheck = false;

                    var filepath = "pack://application:,,,/Image/a未选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");

                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    
                }
                else
                {
                    click.Content = "不选";

                    u.IsCheck = true;

                    var filepath = "pack://application:,,,/Image/a选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                   
                }
            }
        }
       
        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
