﻿using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using NSJL.Framework.Utils;
using DJ.ZEF.ThirdParty;
using DJ.Clients;
using System.Diagnostics;

namespace DJ
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            ComWindow.Start(this);
            //MName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
        }
        //登录
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Label1.Content = "";
            var username = TextBox1.Text;
            var passowrd = PasswordBox1.Password;
            Task.Run(() =>
            {
                var ini = new IniFile("Config/login.ini");
                string account = ini.readKey("Login","account");
                string pwd = ini.readKey("Login","password");
                if (username != account || passowrd != pwd)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Label1.Content = "账号或密码错误";
                        return;
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        Main.UserName = username;
                        try
                        {
                            Main main = Main.GetInstance();
                            //main.Frame1.Content = main.page1;
                            main.ShowDialog();
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageDialog.ShowDialog(ex.Message);
                        }
                    });
                }
            });
        }
        //返回主页
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           this.Close(); 
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }



        /// <summary>
        /// 链接跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = sender as Hyperlink;
            Process.Start(new ProcessStartInfo(link.NavigateUri.AbsoluteUri));
        }
    }
}
