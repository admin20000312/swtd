﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.UserControls
{
    /// <summary>
    /// NowData.xaml 的交互逻辑
    /// </summary>
    public partial class NowData : UserControl
    {
        public NowData()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                var txt = DateTime.Now.ToString("yyyy / MM / dd ");
                Label1.Content = txt + "   " + DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("zh-cn"));
            });
            timer.IsEnabled = true;
            timer.Start();
        }
    }
}
