﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Dialog;
using System.Drawing;
using System.Windows.Interop;
using NSJL.Framework.Utils;

namespace DJ.Third
{
    /// <summary>
    /// Face.xaml 的交互逻辑
    /// </summary>
    public partial class Face : Window
    {
        public Face()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            var result=VideoHelper.GetInstance().Start(ImageWith, Success);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                this.Close();
            }
        }

        public void ImageWith(Bitmap map)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    Image1.Source = ConvertToBitmapSource(map);
                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
        public void Success(byte[] imageBytes)
        {
            try
            {
                Facebytes = (byte[])imageBytes.Clone();
                Dispatcher.Invoke(() =>
                {
                    this.DialogResult = true;
                    this.Close();
                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);   
            }
        }
        public byte[] Facebytes = null;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public static System.Windows.Media.Imaging.BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(btmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }
}
