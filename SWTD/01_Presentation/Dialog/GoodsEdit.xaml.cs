﻿using DJ.Clients;
using NSJL.DAL.DataModel.Entities;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Dialog
{
    /// <summary>
    /// GoodsEdit.xaml 的交互逻辑
    /// </summary>
    public partial class GoodsEdit : Window
    {
        public GoodsEdit()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

         
        }
        private void Window_Closed(object sender, EventArgs e)
        {

        }
      
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(goodsname.Text))
            {
                MessageDialog.ShowDialog("物品名称不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(goodsnum.Text))
            {
                MessageDialog.ShowDialog("物品编号不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(goodsclasss.Text))
            {
                MessageDialog.ShowDialog("物品分类不能为空");
                return;
            }

            this.DialogResult = true;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //选择
        private void Button1_Click4(object sender, RoutedEventArgs e)
        {
            GoodsClassSelect sel=new GoodsClassSelect();
            if (sel.ShowDialog() != true)
            {
                return;
            }
            goodsclasss.Text = sel.GoodsClasss;

        }

        //public string Temp = null;
        //private void Window_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Capital)
        //    {
        //        Temp = "";
        //    }
        //    else if (Temp != null && e.Key != Key.Return)
        //    {
        //        if (e.Key.ToString().StartsWith("D") && e.Key.ToString().Length == 2)
        //        {
        //            var temp = e.Key.ToString().Replace("D", "");
        //            Temp += temp;
        //        }
        //        else
        //        {
        //            var temp = e.Key.ToString();
        //            Temp += temp;
        //        }
        //    }
        //    else if (Temp != null && e.Key == Key.Return)
        //    {
        //        goodsid.Text = Temp;
        //        Temp = null;
        //    }
        //}
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }
        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

    }
}
