﻿using DJ.UserControls;
using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Dialog
{
    /// <summary>
    /// GoodsClassSelect.xaml 的交互逻辑
    /// </summary>
    public partial class GoodsClassSelect : Window
    {
        public GoodsClassSelect()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            Init();
        }
        public void Init()
        {
            var biz = new CommBiz();

            WrapPanel1.Children.Clear();
            var list = biz.GetGoodsClassList();
            foreach (var item in list)
            {
                GoodsTypeControl btn = new GoodsTypeControl();
                btn.Width = 180;
                btn.Height = 80;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Text = item.Name;
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }
        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (GoodsTypeControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                u.Grid1.Background = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#666666");
            }
            else
            {
                u.IsCheck = true;
                u.Grid1.Background = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
            }
        }
        //全选
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button click = (Button)sender;
            var name = (string)click.Content;

            foreach (GoodsTypeControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name != "全选")
                {
                    click.Content = "全选";

                    u.IsCheck = false;
                    u.Grid1.Background = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#666666");
                }
                else
                {
                    click.Content = "不选";
                    u.IsCheck = true;
                    u.Grid1.Background = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private List<string> GetSelectBox()
        {
            List<string> list = new List<string>();
            foreach (GoodsTypeControl item in WrapPanel1.Children)
            {
                if (item.IsCheck)
                {
                    list.Add(item.Label1.Text);
                }
            }
            return list;
        }
        public string GoodsClasss { get; set; }
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                var biz = new CommBiz();
                string str = string.Join(",", list.ToArray());
                GoodsClasss = str;
            }
            else
            {
                MessageDialog.ShowDialog("请选择物品类别");
                return;
            }

            DialogResult = true;
            this.Close();
        }
        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }
    }
}
