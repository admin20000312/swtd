﻿using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using DJ.ZEF.ThirdParty;

namespace NSJL.Biz.Background.Client
{
    public class CommBiz
    {
        public string CabinetName = null;
        public CommBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            CabinetName = ini.readKey("BoxConfig", "cabinetName");
        }
        public SJLDbContext db=new SJLDbContext();
        public CommonResult Login(string username, string password)
        {
            
            var info = db.AdminInfo.Where(p => p.UserName == username).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult(){result = false,message = "用户不存在"};
            }
            if (info.Password != password)
            {
                return new CommonResult() { result = false, message = "密码不正确" };
            }
            return new CommonResult(){result = true};
        }
        public CommonResult EditPassword(string username,string old, string news)
        {
            var db = new SJLDbContext();
            var info = db.AdminInfo.Where(p=>p.UserName==username).FirstOrDefault();
            if (info.Password != old)
            {
                return new CommonResult() { result = false, message = "老密码错误" };
            }

            info.Password = news;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }


        public bool InitBoxInfo(int count,string cabname)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            db.BoxInfo.RemoveRange(list);
            for (int i = 1; i <= count; i++)
            {
                var info = new BoxInfo();
                info.Id = Guid.NewGuid().ToString("N");
                info.CreateTime = DateTime.Now;
                info.BoxNum = i;
                info.BoxType = "小";
                info.IsFree = true;
                info.CabName = cabname;
                db.BoxInfo.Add(info);
            }
            db.SaveChanges();
            return true;
        }




        public List<OpenBoxInfo> GetOpenBoxList(string ope,DateTime? start,DateTime? end)
        {
            var sql = db.OpenBoxInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(ope))
            {
                sql = sql.Where(p => p.Type == ope);
            }
            if (start != null)
            {
                sql = sql.Where(p => p.CreateTime >start);
            }
            if (end != null)
            {
                sql = sql.Where(p => p.CreateTime < end);
            }

            return sql.OrderByDescending(p => p.CreateTime).ToList();

          
        }
        public List<AdminLogInfo> GetAdminLogList(DateTime? start, DateTime? end)
        {
            var db = new SJLDbContext();
            if (start == null)
            {
                return db.AdminLogInfo.OrderByDescending(p => p.CreateTime).ToList();
            }
            else
            {
                return db.AdminLogInfo.Where(p => p.CreateTime >= start && p.CreateTime <= end).OrderByDescending(p => p.CreateTime).ToList();
            }
        }
        public List<BoxInfo> GetBoxList()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p=>p.CabName==CabinetName).OrderBy(p => p.BoxNum).ToList();
            return list;
        }
        public List<GetBoxWithExcel> GetBoxListWithExcel()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.CabName == CabinetName).OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetBoxWithExcel>>(list);
        }
        public List<GetBoxInfo> GetBoxListWithUserId()
        {
            var info = from a in db.BoxInfo
                        join b in db.UserInfo on a.UserId equals b.Id into temp
                        from b in temp.DefaultIfEmpty()
                       where a.CabName== CabinetName
                       select new GetBoxInfo()
                        {
                            Id = a.Id,
                            CabName = a.CabName,
                            BoxNum = a.BoxNum,
                            IsLock = a.IsLock,
                            UserId = a.UserId,
                            IsFree = a.IsFree,
                            IsBind = a.IsBind,
                            GoodsName = a.GoodsName,
                            //GoodsId = a.GoodsId,
                            Name = b.Name,
                            Mobile = b.Mobile,
                            BoxType=a.BoxType,
                       };
            return info.OrderBy(p => p.BoxNum).ToList();
        }
        public List<GetBoxInfo> GetBoxListWithBoxId(List<int> blist,string userid)
        {
            var info = from a in db.BoxInfo
                join b in db.UserInfo on a.UserId equals b.Id into temp
                from b in temp.DefaultIfEmpty()
                where a.CabName == CabinetName && a.IsLock != true && (a.UserId==null||a.UserId=="" ||a.UserId==userid)
                select new GetBoxInfo()
                {
                    Id = a.Id,
                    CabName = a.CabName,
                    BoxNum = a.BoxNum,
                    IsLock = a.IsLock,
                    UserId = a.UserId,
                    IsFree = a.IsFree,
                    IsBind = a.IsBind,
                    GoodsName = a.GoodsName,
                    //Name = b.Name,
                    //Mobile = b.Mobile,
                    //BoxNums = b.BoxNums,
                    BoxType = a.BoxType,
                };
            var list= info.OrderBy(p => p.BoxNum).ToList();
            return list.Where(p => blist.Contains(p.BoxNum)).ToList();
        }
        public List<GetBoxInfo> GetBoxListWithUserIdClient(string userid)
        {
            var info = from a in db.BoxInfo
                join b in db.UserInfo on a.UserId equals b.Id into temp
                from b in temp.DefaultIfEmpty()
                where a.CabName == CabinetName && a.IsLock != true && a.UserId==userid
                select new GetBoxInfo()
                {
                    Id = a.Id,
                    CabName = a.CabName,
                    BoxNum = a.BoxNum,
                    IsLock = a.IsLock,
                    UserId = a.UserId,
                    IsFree = a.IsFree,
                    IsBind = a.IsBind,
                    GoodsName = a.GoodsName,
                    //Name = b.Name,
                    //Mobile = b.Mobile,
                    //BoxNums = b.BoxNums,
                    BoxType = a.BoxType,
                };
            var list = info.OrderBy(p => p.BoxNum).ToList();
            return list;
        }
        public void UnLockUserInfo(string id)
        {
            var info = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();
            info.BoxNums = "";
            db.SaveChanges();
        }
        public List<GetGoodsList> GetBoxListWithLin()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetGoodsList>>(list);
        }
        public bool EditAdminLogInfo(string str,string adminname)
        {
            var db = new SJLDbContext();
            var info = new AdminLogInfo();
            info.UserName = adminname;
            info.CreateTime = DateTime.Now;
            info.Content = str;
            info.Id = Guid.NewGuid().ToString("N");
            info.CabName = CabinetName;
            db.AdminLogInfo.Add(info);
            db.SaveChanges();
            return true;
        }
        public CommonResult ClearBoxList(List<int> tlist)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.CabName == CabinetName).ToList();
            foreach (var item in tlist)
            {
                var info = list.Where(p => p.BoxNum == item).FirstOrDefault();
                info.IsBind = false;
                info.IsFree = true;
                //info.GoodsId = null;
                info.GoodsName = null;
                info.UserId = null;

                info.GoodsNum = null;
                info.GoodsClasss = null;

                info.BindTime = null;
            }
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public bool AllClearBoxinfo()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            foreach (var item in list)
            {
                item.IsFree = true;
                item.GoodsName = null;
                item.IsBind = false;
                //item.GoodsId = null;
                item.UserId = null;

                item.GoodsNum = null;
                item.GoodsClasss = null;

                item.BindTime = null;
            }
            db.SaveChanges();
            return true;
        }
        public bool LockBoxList(List<int> tlist)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.CabName == CabinetName).ToList();
            foreach (var item in tlist)
            {
                var info = list.Where(p => p.BoxNum == item).FirstOrDefault();
                info.IsLock = true;
            }
            db.SaveChanges();
            return true;
        }
        public bool UnLockBoxList(List<int> tlist)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.CabName == CabinetName).ToList();
            foreach (var item in tlist)
            {
                var info =list.Where(p => p.BoxNum == item).FirstOrDefault();
                info.IsLock = false;
            }
            db.SaveChanges();
            return true;
        }
        public List<GetUserInfo> GetUserList(string name)
        {
            var db = new SJLDbContext();
            if (string.IsNullOrWhiteSpace(name))
            {
                var list= db.UserInfo.OrderByDescending(p => p.CreateTime).ToList();

                return AutoMapper.Mapper.DynamicMap<List<GetUserInfo>>(list);
            }
            else
            {
                var list = db.UserInfo.Where(p => p.Name.Contains(name)).OrderByDescending(p => p.CreateTime).ToList();
                return AutoMapper.Mapper.DynamicMap<List<GetUserInfo>>(list);
            }
        }
        public bool EditBoxType(List<int> list, string type)
        {
            var db = new SJLDbContext();
            foreach (var item in list)
            {
                var info = db.BoxInfo.Where(p => p.BoxNum == item).FirstOrDefault();
                info.BoxType = type;
            }
            db.SaveChanges();
            return true;
        }
        public bool DelUserInfo(string id)
        {
            var info = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();
            db.UserInfo.Remove(info);
            db.SaveChanges();
            return true;
        }
        public CommonResult EditUserInfo(string id, string name, string mobile, string code,string fea,string goodsclasss,string faceId = null,string faceBytes = null,string pwd = null)
        {
            var db = new SJLDbContext();
            var list = db.UserInfo.Where(p => p.Features != null && p.Features != "").OrderByDescending(p => p.CreateTime).ToList();
            if (string.IsNullOrWhiteSpace(id))
            {
                if (db.UserInfo.Any(p => p.Code == code))
                {
                    return new CommonResult(){result = false,message = "卡号已存在，无法添加"};
                }
                if (db.UserInfo.Any(p => p.FaceId == faceId))
                {
                    return new CommonResult() { result = false, message = "人脸已存在，无法添加" };
                }
                var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Features).ToList(), fea);
                if (ints >= 0)
                {
                    return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                }
                var info = new UserInfo();
                info.Id = Guid.NewGuid().ToString("N");
                info.CreateTime = DateTime.Now;
                info.UpdateTime = DateTime.Now;
                info.Name = name;
                info.Mobile = mobile;
                info.Code = code;
                info.Features = fea;
                info.GoodsClasss = goodsclasss;
                info.FaceId = faceId;
                info.HeadPic = faceBytes;
                
                info.Password = pwd;
                db.UserInfo.Add(info);
            }
            else
            {
                var user = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();

                if (db.UserInfo.Any(p => p.Code == code) && user.Code!=code)
                {
                    return new CommonResult() { result = false, message = "卡号已存在，无法添加" };
                }

                var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Features).ToList(), fea);
                if (ints >= 0)
                {
                    if (list[ints].Id != id)
                    {
                        return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                    }
                }

                user.Name = name;
                user.Mobile = mobile;
                user.Code = code;
                user.Features = fea;
                user.UpdateTime=DateTime.Now;
                user.GoodsClasss = goodsclasss;
                if (!string.IsNullOrWhiteSpace(faceId) && !string.IsNullOrWhiteSpace(faceBytes))
                {
                    user.FaceId = faceId;
                    user.HeadPic = faceBytes;
                }
                user.Password = pwd;
            }
            db.SaveChanges();
            
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public bool IsFace(string faceId)
        {
            return db.UserInfo.Any(p => p.FaceId == faceId);
        }
  
        public CommonResult EditCabinet(string name)
        {
            if (db.CabinetInfo.Any(p => p.Name == name))
            {
                return new CommonResult(){result = false,message = "柜号名已存在"};
            }
            var info=new CabinetInfo();
            info.Id = Guid.NewGuid().ToString("N");
            info.Name = name;
            info.CreateTime=DateTime.Now;
            db.CabinetInfo.Add(info);
            db.SaveChanges();
            return new CommonResult(){result = true};
        }
        public List<CabinetInfo> GetCabinetList()
        {
            return db.CabinetInfo.OrderByDescending(p=>p.CreateTime).ToList();
        }
        public bool DelCabinet(string name)
        {
            var info = db.CabinetInfo.Where(p => p.Name == name).FirstOrDefault();
            db.CabinetInfo.Remove(info);
            db.SaveChanges();
            return true;
        }
        public bool IsCabinet(string name)
        {
            return db.CabinetInfo.Any(p => p.Name == name);
        }
        public List<LogInfo> GetLogList(string caname=null,string type=null)
        {
            var sql = db.LogInfo.AsQueryable();
            if (!string.IsNullOrWhiteSpace(caname))
            {
                sql = sql.Where(p => p.CabName == caname);
            }
            if (!string.IsNullOrWhiteSpace(type))
            {
                sql = sql.Where(p => p.Type == type);
            }

            return sql.OrderByDescending(p => p.CreateTime).ToList();
        }
        public CommonResult EditGoods(string id,string goodsname,string goodsnum,string goodsclasss)
        {
            var info = db.BoxInfo.Where(p => p.Id == id).FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(info.UserId))
            {
                return new CommonResult() { result = false, message = "已借出的物品不能编辑" };
            }
            //if (db.BoxInfo.Where(p => p.CabName == CabinetName && p.Id != id).Any(p => p.GoodsId == goodsid))
            //{
            //    return new CommonResult() { result = false, message = "标签号已存在" };
            //}
            if (db.BoxInfo.Where(p => p.CabName == CabinetName && p.Id != id).Any(p => p.GoodsNum == goodsnum))
            {
                return new CommonResult() { result = false, message = "物品编号已存在" };
            }

            info.GoodsNum = goodsnum;
            info.GoodsClasss = goodsclasss;
            info.IsBind = true;
            info.IsFree = false;
            info.GoodsName = goodsname;
            //info.GoodsId = goodsid;
            info.BindTime=DateTime.Now;
            db.SaveChanges();
            return new CommonResult(){result = true};
        }
        public CommonResult<UserInfo> GetUserListWithPassword(string ps)
        {
            var info = db.UserInfo.Where(p => p.Password == ps).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<UserInfo>(){result = false,message = "密码错误"};
            }
            return new CommonResult<UserInfo>(){result = true,data = info};
        }
        public CommonResult<UserInfo> GetUserInfoWithFaceId(string faceId)
        {
            var info = db.UserInfo.Where(p => p.FaceId == faceId).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<UserInfo>() { result = false, message = "用户不存在" };
            }
            return new CommonResult<UserInfo>(){result = true,data = info};
        }
        public bool EditBoxGoodsNum(UserInfo user,int boxnum, int num)
        {
            var info = db.BoxInfo.Where(p => p.CabName == CabinetName && p.BoxNum == boxnum).FirstOrDefault();

            var model=new OpenBoxInfo();
            model.Id = Guid.NewGuid().ToString("N");
            model.CreateTime=DateTime.Now;
            model.CabName = CabinetName;
            model.BoxNum = boxnum;
            model.Name = user.Name;
            if (num > 0)
            {
                model.Type = "存";
                model.State = "补库";
                model.Num = num;
            }
            else
            {
                model.Type = "取";
                model.State = "领料";
                model.Num = num * -1;
            }
            db.OpenBoxInfo.Add(model);

            db.SaveChanges();
            return true;
        }
        //public List<BoxInfo> GetBoxListWithBind()
        //{
        //    return db.BoxInfo.Where(p => p.GoodsId != null && p.GoodsId!="").OrderBy(p=>p.BoxNum).ToList();
        //}
        public CommonResult ReturnWorker(int num,UserInfo info)
        {
            var list = db.BoxInfo.Where(p => p.BoxType == "退料").ToList();
            if (list.Count == 0)
            {
                return new CommonResult(){result = false};
            }
            var boxInfo = list.OrderBy(p => Guid.NewGuid()).FirstOrDefault();

            var model = new OpenBoxInfo();
            model.Id = Guid.NewGuid().ToString("N");
            model.CreateTime = DateTime.Now;
            model.CabName = CabinetName;
            model.BoxNum = boxInfo.BoxNum;
            model.Name = info.Name;
            model.Type = "存";
            model.State = "退料";
            model.Num = num;
            db.OpenBoxInfo.Add(model);

            db.SaveChanges();
            return new CommonResult(){result = true,data = boxInfo.BoxNum.ToString()};
        }
        public void MoveBoxInfo(UserInfo user,BoxInfo info,int num)
        {
            var box = db.BoxInfo.Where(p => p.CabName == CabinetName && p.BoxNum ==info.BoxNum ).FirstOrDefault();

            var model = new OpenBoxInfo();
            model.Id = Guid.NewGuid().ToString("N");
            model.CreateTime = DateTime.Now;
            model.CabName = CabinetName;
            model.BoxNum = info.BoxNum;
            model.Name = user.Name;
            model.Type = "取";
            model.State = "移库";
            model.Num = num;
            db.OpenBoxInfo.Add(model);

            db.SaveChanges();
            return;
        }
        public void EditUserInfoWithBoxNums(string id, string boxs)
        {
            var user = db.UserInfo.Where(p => p.Id == id).FirstOrDefault();
            user.BoxNums = boxs;
            db.SaveChanges();

        }
        public UserInfo GetUserInfoWithCode(string code)
        {
            var db = new SJLDbContext();
            return db.UserInfo.Where(p => p.Code == code).FirstOrDefault();
        }

        private int num = 0;

        public CommonResult ImportExcelWithGoods(DataTable dt)
        {
            var list = db.BoxInfo.ToList();
            var glist = db.GoodsClassInfo.ToList();
            var templist = db.GoodsClassInfo.Select(p => p.Name).ToList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var temp = (string)dt.Rows[i][0];
                var temp1 = Convert.ToInt32(dt.Rows[i][1]);
                var temp2 = (string)dt.Rows[i][2];
                //var temp3 = (string)dt.Rows[i][3];
                var temp4 = (string)dt.Rows[i][3];
                var temp5 = (string)dt.Rows[i][4];
                if (temp == CabinetName)
                {
                    if (!string.IsNullOrWhiteSpace(temp4))
                    {
                        if (list.Where(p => p.BoxNum != temp1).Any(p => p.GoodsNum == temp4))
                        {
                            continue;
                        }
                    }
                    //if (!string.IsNullOrWhiteSpace(temp3))
                    //{
                    //    if (list.Where(p => p.BoxNum != temp1).Any(p => p.GoodsId == temp3))
                    //    {
                    //        continue;
                    //    }
                    //}
                    var info = list.Where(p => p.CabName == temp && p.BoxNum == temp1).FirstOrDefault();
                    info.GoodsName = temp2;
                    //info.GoodsId = temp3;
                    info.GoodsNum = temp4;
                    info.GoodsClasss = temp5 == null ? null : temp5.Replace("，", ",");

                    if (info.GoodsClasss != null)
                    {
                        var arr = info.GoodsClasss.Split(',');
                        foreach (var item in arr)
                        {
                            if (!string.IsNullOrWhiteSpace(item))
                            {
                                var tempsss = item.Trim();

                                if (!templist.Any(p => p == tempsss))
                                {
                                    templist.Add(tempsss);

                                    var model = new GoodsClassInfo();
                                    model.CreateTime = DateTime.Now;
                                    model.Id = Guid.NewGuid().ToString("N");
                                    model.Name = tempsss;
                                    db.GoodsClassInfo.Add(model);
                                }
                            }
                        }
                    }
                    num++;
                }
            }
            if (num<=0)
            {
                return new CommonResult() { result = false,message = "导入失败，只能导入同柜号箱门数据" };
            }
            db.SaveChanges();
            return new CommonResult() { result = true ,message = "导入成功"};
            }





        public CommonResult<UserInfo> PwdYZ(string code, string password)
        {

            var info = db.UserInfo.Where(p => p.Code == code).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<UserInfo>() { result = false, message = "用户不存在" };
            }
            if (info.Password != password)
            {
                return new CommonResult<UserInfo>() { result = false, message = "密码不正确" };
            }
            return new CommonResult<UserInfo>() { result = true, data = info};
        }


        public List<GetBoxWithExcelOther> GetUserListWithExcel()
        {
            var list = db.UserInfo.OrderByDescending(p => p.CreateTime).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetBoxWithExcelOther>>(list);
        }
        public void ImportExcelWithUserInfo(DataTable dt)
        {
            var list = db.UserInfo.ToList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var temp = (string)dt.Rows[i][0];
                var temp1 = (string)(dt.Rows[i][1] == DBNull.Value ? "" : dt.Rows[i][1]);
                var temp2 = (string)(dt.Rows[i][2] == DBNull.Value ? "" : dt.Rows[i][2]);
                var temp3 = (string)(dt.Rows[i][3] == DBNull.Value ? "" : dt.Rows[i][3]);
                var temp4 = (string)(dt.Rows[i][4] == DBNull.Value ? "" : dt.Rows[i][4]);

                if (!string.IsNullOrWhiteSpace(temp))
                {
                    if (list.Any(p => p.Code == temp3))
                    {
                        continue;
                    }
                    var info = list.Where(p => p.Id == temp).FirstOrDefault();
                    if (info != null)
                    {
                        info.Name = temp1;
                        info.Mobile = temp2;
                        info.Code = temp3;
                        info.GoodsClasss = temp4.Replace(" ", "").Replace("，", ",");
                    }
                    else
                    {
                        var model = new UserInfo();
                        model.Id = temp;
                        model.CreateTime = DateTime.Now;
                        model.Name = temp1;
                        model.Mobile = temp2;
                        model.Code = temp3;
                        model.GoodsClasss = temp4.Replace(" ", "").Replace("，", ",");
                        db.UserInfo.Add(model);
                    }
                }
            }
            db.SaveChanges();

        }
        public void EditUserBox(UserInfo user, int boxnum,string type)
        {
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            if (type == "借")
            {
                info.UserId = user.Id;
            }
            else
            {
                info.UserId = null;
            }

            var model=new OpenBoxInfo();
            model.Id = Guid.NewGuid().ToString("N");
            model.CreateTime=DateTime.Now;
            model.Type = type;
            model.CabName = CabinetName;
            model.BoxNum = boxnum;
            model.Name = user.Name;
            model.Code = user.Code;
            db.OpenBoxInfo.Add(model);
            db.SaveChanges();
            return;
        }
        public void SetBoxTypeList(List<int> tlist, string type)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.CabName == CabinetName).ToList();
            foreach (var item in tlist)
            {
                var info = list.Where(p => p.BoxNum == item).FirstOrDefault();
                info.BoxType = type;
            }
            db.SaveChanges();
        }
        //public void EditRFIDInfo(int boxnum)
        //{
        //    var info = db.BoxInfo.Where(p => p.CabName == CabinetName && p.BoxNum == boxnum).FirstOrDefault();

        //    var model=new RFIDInfo();
        //    model.Id = Guid.NewGuid().ToString("N");
        //    model.CreateTime=DateTime.Now;
        //    model.BoxNum = boxnum;
        //    model.GoodsId = info.GoodsId;
        //    model.GoodsName = info.GoodsName;
        //    model.UserId = info.UserId;
        //    model.CabName = CabinetName;
        //    db.RFIDInfo.Add(model);
        //    db.SaveChanges();
        //}
        public CommonResult EditGoodsClass(string id,string name)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                if (db.GoodsClassInfo.Any(p => p.Name == name))
                {
                    return new CommonResult(){result = false,message = "名称已存在"};
                }
                var model = new GoodsClassInfo();
                model.Id = Guid.NewGuid().ToString("N");
                model.CreateTime = DateTime.Now;
                model.Name = name;
                db.GoodsClassInfo.Add(model);
            }
            else
            {
                if (db.GoodsClassInfo.Where(p=>p.Id!=id).Any(p => p.Name == name))
                {
                    return new CommonResult() { result = false, message = "名称已存在" };
                }
                var info = db.GoodsClassInfo.Where(p => p.Id == id).FirstOrDefault();
                info.Name = name;
                info.CreateTime=DateTime.Now;
            }
            db.SaveChanges();
            return new CommonResult(){result = true,message = "操作成功"};
        }
        public CommonResult DelGoodsClass(string id)
        {
            var info = db.GoodsClassInfo.Where(p => p.Id == id).FirstOrDefault();
            var name = info.Name;
            var ulist=db.UserInfo.Where(p => p.GoodsClasss.Contains(name)).ToList();

            foreach (var item in ulist)
            {
                if (item.GoodsClasss != null)
                {
                    var gname = item.GoodsClasss.Split(',');
                    foreach (var aitem in gname)
                    {
                        if (name == aitem)
                        {
                            return new CommonResult() { result = false, message = "该类别已被用户占用，无法删除" };
                        }
                    }
                }
            }

            db.GoodsClassInfo.Remove(info);
            db.SaveChanges();
            return new CommonResult() { result = true, message = "删除成功" };
        }
        public List<GetGoodsClassList> GetGoodsClassList()
        {
            return AutoMapper.Mapper.DynamicMap<List<GetGoodsClassList>>(db.GoodsClassInfo
                .OrderByDescending(p => p.CreateTime).ToList());
        }
        public List<GetBoxInfo> GetBoxInfoWithGoodsClass(string goodsclasss)
        {
            var glist = goodsclasss.Split(',');
            var list=db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            var clist = AutoMapper.Mapper.DynamicMap<List<GetBoxInfo>>(list);
            var NoList = clist.Where(p => p.CabName != CabinetName).ToList();
            foreach(var  c in NoList)
            {
                clist.Remove(c);
            }
            foreach (var item in clist)
            {
                if (item.GoodsClasss != null)
                {
                    item.GoodsClass = item.GoodsClasss.Split(',').ToList();
                }
                else
                {
                    item.GoodsClass=new List<string>();
                }
            }
            var newbox=new List<GetBoxInfo>();
            foreach (var item in glist)
            {
                var temp = clist.Where(p => p.GoodsClass.Contains(item)).ToList();
                newbox.AddRange(temp);
            }
            return newbox.Distinct().OrderBy(p => p.BoxNum).ToList();
        }
        public List<UserInfo> GetUserInfoWithF()
        {
            return db.UserInfo.Where(p=>p.Features!=null && p.Features!="").OrderByDescending(p => p.CreateTime).ToList();
        }
        public void EditOpenBoxInfo(UserInfo user, int boxnum, string type,int SelectType)
        {
            var info = db.BoxInfo.Where(p => p.CabName == CabinetName && p.BoxNum == boxnum).FirstOrDefault();
            var model=new OpenBoxInfo();
            model.Id = Guid.NewGuid().ToString("N");
            model.CabName = CabinetName;
            model.CreateTime=DateTime.Now;
            model.BoxNum = boxnum;
            model.Type = type;
            model.GoodsName = info.GoodsName;
            //model.GoodsId = info.GoodsId;
            model.GoodsNum = info.GoodsNum;
            model.Name = user.Name;
            if (SelectType == 1)//人脸
            {
                model.HeadPic = user.HeadPic;
            }
            else if(SelectType == 2)//指纹
            {
                model.Features = user.Features;
                //model.Code = user.Code;
            }
            else if(SelectType == 3)//密码
            {
                model.Code = user.Code;
            }
            if (type == "借")
            {
                info.UserId = user.Id;
            }
            else if(type == "还")
            {
                info.UserId = null;
            }
            info.UpdateTime=DateTime.Now;
            db.OpenBoxInfo.Add(model);
            db.SaveChanges();
        }


        public CommonResult Deposit(UserInfo user, int boxnum, List<RFIDHelper.RFIDInfo> list, string type = "还")
        {
            try
            {
                if (list != null)
                {
                    string str = string.Join(",", list.Select(p => p.LabelId).ToArray());
                    TextLogUtil.Info("箱门：" + boxnum + ",盘到的标签：" + str);
                }
                var db = new SJLDbContext();
                if (type == "还")
                {
                    //if (list.Any(p => p.LabelId == user.RFIDNum))
                    //{
                        var box = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();

                        //添加记录归还成功
                        var model = new OpenBoxInfo();
                        model.Id = Guid.NewGuid().ToString("N");
                        model.CreateTime = DateTime.Now;
                        model.Type = type;
                        //model.RFIDNum = user.RFIDNum;
                        model.BoxNum = boxnum;
                        model.Name = user.Name;
                        model.CabName = CabinetName;
                        model.GoodsName = box.GoodsName;
                        model.Features = user.Features;
                        model.GoodsNum = box.GoodsNum;
                        model.Code = user.Code;
                        db.OpenBoxInfo.Add(model);
                        db.SaveChanges();
                        return new CommonResult() { result = true, message = "归还成功" };
                    }
                    //else
                    //{
                    //    return new CommonResult() { result = false, message = "检测不到标签" };
                    //}
                //}
                else
                {
                    //借出
                    //if (!list.Any(p => p.LabelId == user.RFIDNum))
                    //{
                        var box = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
                        //添加记录借出成功
                        var model = new OpenBoxInfo();
                        model.Id = Guid.NewGuid().ToString("N");
                        model.CreateTime = DateTime.Now;
                        model.Type = type;
                        //model.RFIDNum = user.RFIDNum;
                        model.BoxNum = boxnum;
                        model.Code = user.Code;
                        model.Name = user.Name;
                        model.CabName = CabinetName;
                        model.GoodsName = box.GoodsName;
                        model.Features = user.Features;
                        model.GoodsNum = box.GoodsNum;
                        db.OpenBoxInfo.Add(model);
                        db.SaveChanges();
                        return new CommonResult() { result = true, message = "借出成功" };
                    //}
                    //else
                    //{
                    //    return new CommonResult() { result = false, message = "未借出物品" };
                    //}
                }
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                TextLogUtil.Info("写入数据库失败" + e.Message);
                return new CommonResult() { result = true, message = e.Message };
            }

        }


        public List<GetBoxInfo> GetHuanBoxList(UserInfo user)
        {

            var list = db.BoxInfo.Where(p => p.UserId == user.Id).OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetBoxInfo>>(list);
        }
        //public bool OnPolice(string labelId)
        //{
        //    var db = new SJLDbContext();
        //    var info = db.BoxInfo.Where(p => p.GoodsId == labelId).FirstOrDefault();
        //    if (info != null)
        //    {
        //        var open=db.OpenBoxInfo.Where(p => p.GoodsId == labelId).OrderByDescending(p => p.CreateTime).FirstOrDefault();
        //        if (open == null || open.Type == "还")
        //        {
        //            var time = DateTime.Now.AddMinutes(-1);
        //            if (!db.RFIDInfo.Any(p => p.CreateTime > time))
        //            {
        //                var model = new RFIDInfo();
        //                model.CabName = CabinetName;
        //                model.CreateTime = DateTime.Now;
        //                model.Id = Guid.NewGuid().ToString("N");
        //                model.GoodsName = info.GoodsName;
        //                model.GoodsId = info.GoodsId;
        //                model.GoodsNum = info.GoodsNum;
        //                model.BoxNum = info.BoxNum;
        //                db.RFIDInfo.Add(model);
        //                db.SaveChanges();
        //            }
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        //public List<RFIDInfo> GetRFIDList()
        //{
        //    var db=new SJLDbContext();
        //    return db.RFIDInfo.OrderByDescending(p => p.CreateTime).ToList();-
        //}
        public void ReturnBoxList(List<int> tlist)
        {
            foreach (var item in tlist)
            {
                var info = db.BoxInfo.Where(p => p.CabName == CabinetName && p.BoxNum == item).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(info.UserId))
                {
                    var model = new OpenBoxInfo();
                    model.Id = Guid.NewGuid().ToString("N");
                    model.CabName = CabinetName;
                    model.CreateTime = DateTime.Now;
                    model.BoxNum = item;
                    model.Type = "还";
                    model.GoodsName = info.GoodsName;
                    //model.GoodsId = info.GoodsId;
                    model.GoodsNum = info.GoodsNum;
                    model.Name = "";
                    info.UserId = null;
                    info.UpdateTime = DateTime.Now;
                    db.OpenBoxInfo.Add(model);
                }
            }
            db.SaveChanges();
        }



        #region old
        public AdminInfo GetAdminInfo()
        {
            var db = new SJLDbContext();
            return db.AdminInfo.FirstOrDefault();
        }
        public List<BoxInfo> GetBoxListWithName(string name)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.Where(p => p.GoodsName.Contains(name)).OrderBy(p => p.BoxNum).ToList();
            return list;
        }
        public BoxInfo GetBoxInfo(int boxnum)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            return info;
        }
        public CommonResult ClearBoxInfo(int boxnum)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();

            info.GoodsName = null;
            info.IsBind = false;
            info.IsFree = true;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }
        public bool LockBoxInfo(int boxnum)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            info.IsLock = true;
            db.SaveChanges();
            return true;
        }
        public bool UnLockBoxInfo(int boxnum)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            info.IsLock = false;
            db.SaveChanges();
            return true;
        }
        public bool CompulsoryReturn(int boxnum)
        {
            var db = new SJLDbContext();
            var box = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            var user = db.UserInfo.Where(p => p.Id == box.UserId).FirstOrDefault();

            var info = new OpenBoxInfo();
            info.Id = Guid.NewGuid().ToString("N");
            info.CreateTime = DateTime.Now;
            info.BoxNum = box.BoxNum;
            info.GoodsName = box.GoodsName;
            info.Type = "归还";
            info.Name = user.Name;
            info.Code = user.Code;
            db.OpenBoxInfo.Add(info);

            box.UserId = null;
            db.SaveChanges();
            return true;
        }
        public bool BindBoxGoods(int boxnum, string goodsname)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();

            info.GoodsName = goodsname;
            info.CreateTime = DateTime.Now;
            info.IsBind = true;
            info.IsFree = false;
            db.SaveChanges();
            return true;
        }
        public List<UserInfo> GetUserListWithFing()
        {
            var db = new SJLDbContext();
            return db.UserInfo.Where(p => p.FingNo != null && p.FingNo != "").OrderByDescending(p => p.CreateTime).ToList();
        }
        //public bool DelUserInfo(string name)
        //{
        //    var db = new SJLDbContext();
        //    if (string.IsNullOrWhiteSpace(name))
        //    {
        //        var list = db.UserInfo.OrderByDescending(p => p.CreateTime).ToList();
        //        db.UserInfo.RemoveRange(list);
        //        db.SaveChanges();
        //        return true;
        //    }
        //    else
        //    {
        //        var info = db.UserInfo.Where(p => p.Name == name).FirstOrDefault();
        //        if (info == null)
        //        {
        //            return false;
        //        }

        //        db.UserInfo.Remove(info);
        //        db.SaveChanges();
        //        return true;
        //    }
        //}


        
        public bool ImportExcel(DataTable dt)
        {
            var db = new SJLDbContext();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var temp = dt.Rows[i][0];
                var boxnum = Convert.ToInt32(temp);

                var waringnum = dt.Rows[i][1].ToString();
                var num = dt.Rows[i][2].ToString();
                var goodsname = dt.Rows[i][3].ToString();

                var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
                info.GoodsName = goodsname;

                if (!string.IsNullOrWhiteSpace(info.GoodsName))
                {
                    info.IsFree = false;
                    info.IsBind = true;
                }
            }
            db.SaveChanges();
            return true;
        }
        
        public List<BoxInfo> GetBoxInfoWithUserId(string userid)
        {
            var db = new SJLDbContext();
            return db.BoxInfo.Where(p => p.UserId == userid).OrderBy(p => p.BoxNum).ToList();
        }
        public CommonResult GoodsLend(UserInfo user, int boxnum, string code, string fingno, int num)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            info.UpdateTime = DateTime.Now;
            info.UserId = user.Id;

            var open = new OpenBoxInfo();
            open.Id = Guid.NewGuid().ToString("N");
            open.CreateTime = DateTime.Now;
            open.Name = user.Name;
            open.BoxNum = boxnum;
            open.Type = "入库";
            open.GoodsName = info.GoodsName;

            open.Code = code;
            open.FingNo = fingno;
            open.Num = num;

            db.OpenBoxInfo.Add(open);
            db.SaveChanges();
            return new CommonResult() { result = true };

        }
        public CommonResult GoodsReturn(UserInfo user, int boxnum, string code, string fingno, int num)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxnum).FirstOrDefault();
            info.UpdateTime = DateTime.Now;
            info.UserId = null;

            var open = new OpenBoxInfo();
            open.Id = Guid.NewGuid().ToString("N");
            open.CreateTime = DateTime.Now;
            open.Name = user.Name;
            open.BoxNum = boxnum;
            open.Type = "领用";
            open.GoodsName = info.GoodsName;

            open.Code = code;
            open.FingNo = fingno;
            open.Num = num;

            db.OpenBoxInfo.Add(open);
            db.SaveChanges();
            return new CommonResult() { result = true };
        }
        public bool IsLend(int boxid)
        {
            var db = new SJLDbContext();
            var info = db.BoxInfo.Where(p => p.BoxNum == boxid).Select(p => p.UserId).FirstOrDefault();
            return !string.IsNullOrWhiteSpace(info);
        }
        #endregion

        #region

        public class FaceFace
        {
            public decimal sorce { get; set; }
            public UserInfo user { get; set; }
        }

        //public CommonResult<UserInfo> GetUserInfoWithFaceFea(string fea)
        //{
        //    var db = new SJLDbContext();
        //    var list = db.UserInfo.Where(p => p.Features != null).ToList();

        //    List<FaceFace> tUser = new List<FaceFace>();
        //    foreach (var item in list)
        //    {
        //        var soce = BaiduFaceApiHelper.GetInstance().CompareFeature(fea, item.Features);
        //        tUser.Add(new FaceFace() { sorce = soce, user = item });
        //    }

        //    var temp = tUser.OrderByDescending(p => p.sorce).FirstOrDefault();
        //    if (temp == null || temp.sorce < 80)
        //    {
        //        return new CommonResult<UserInfo>() { result = false, message = "用户不存在" };
        //    }

        //    return new CommonResult<UserInfo>() { result = true, data = temp.user };
        //}
        #endregion

    }
}
