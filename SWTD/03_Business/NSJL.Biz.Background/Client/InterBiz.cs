﻿using NSJL.DAL.DataModel.Models;
using NSJL.DomainModel.Background.Client;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NSJL.Biz.Background.Client
{
    /// <summary>
    /// 本地管理服务
    /// </summary>
    /// 
    public class InterBiz
    {
        public static string username = null;
        public static string userpwd = null;
        public static string url = null;
        public static string Cabinet = null;
       
        static InterBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            username = ini.readKey("InterfaceServer", "username");
            userpwd = ini.readKey("InterfaceServer", "userpwd");
            Cabinet = ini.readKey("InterfaceServer", "Cabinet");
            url = ini.readKey("InterfaceServer", "url");
        }

        public static string TokenStr { get; set; }
        public static CommonResult Login(string name, string pwd)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "UserLogin/Login?name=" + name + "&pwd=" + pwd, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult() { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult() { result = false, message = data.Message };
                }

                TokenStr = data.Data.ToString();
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                return new CommonResult() { result = false, message = e.Message };
            }
        }

        #region 箱柜接口
        public static CommonResult<List<CabinetInfo>> GetAllCabinet()
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Cabinet/selectAll", null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<List<CabinetInfo>> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<List<CabinetInfo>>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<List<CabinetInfo>>() { result = false, message = data.Message };
                }
                return new CommonResult<List<CabinetInfo>>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<List<CabinetInfo>>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> AddCabinet(string name)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Cabinet/AddCabinet?name=" + name, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> DeleteCabinet(string id)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Cabinet/DeleteCabinet?id=" + id, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> EditCabinet(string id, string name)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Cabinet/EditCabinet?id=" + id + "&name=" + name, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        #endregion

        #region 箱格接口
        //获取所有箱格
        public static CommonResult<List<BoxInfo>> GetAllBox()
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Box/selectAll", null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<List<BoxInfo>> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<List<BoxInfo>>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<List<BoxInfo>>() { result = false, message = data.Message };
                }
                return new CommonResult<List<BoxInfo>>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<List<BoxInfo>>() { result = false, message = e.Message };
            }
        }
        
        public static CommonResult<List<BoxInfo>> GetBoxByCab(string cabN)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Box/SelectBoxByCab?cabName="+ cabN, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<List<BoxInfo>> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<List<BoxInfo>>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<List<BoxInfo>>() { result = false, message = data.Message };
                }
                return new CommonResult<List<BoxInfo>>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<List<BoxInfo>>() { result = false, message = e.Message };
            }
        }

        //初始化箱门
        public static CommonResult<CommonResult1> InitBox(string cabNum, int num)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "Box/InitBox?cabNum=" + cabNum + "&num=" + num, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }
        
        //编辑箱门
        public static CommonResult<CommonResult1> EditBox(BoxInfo box)
        {
            try
            {
                var flag = 0;
                var json = new JavaScriptSerializer().Serialize(box);
            ReStart:
                var result = GetRequest(url + "Box/EditBox", json, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }



        #endregion


        #region 存取记录接口
        //获取所有存取记录
        public static CommonResult<List<AccessLog>> GetAllAccess()
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "AccessLog/selectAll", null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<List<AccessLog>> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<List<AccessLog>>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<List<AccessLog>>() { result = false, message = data.Message };
                }
                return new CommonResult<List<AccessLog>>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<List<AccessLog>>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> AddAccessLog(int style,AccessLog al)//style 0:服务只设置存入时间，1：服务设置取出送样时间
        {
            try
            {
                var flag = 0;
                var json = new JavaScriptSerializer().Serialize(al);
            ReStart:
                var result = GetRequest(url + "AccessLog/AddAccessLog?type="+ style, json, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }


        #endregion


        #region 样品箱接口
        //获取所有样品箱
        public static CommonResult<List<SampleBoxInfo>> GetAllSampleBox()
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "SampleBox/selectAll", null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<List<SampleBoxInfo>> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<List<SampleBoxInfo>>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<List<SampleBoxInfo>>() { result = false, message = data.Message };
                }
                return new CommonResult<List<SampleBoxInfo>>() { result = true, data = data.Data };
            }
            catch (Exception e)
            {
                return new CommonResult<List<SampleBoxInfo>>() { result = false, message = e.Message };
            }
        }
        public static CommonResult<CommonResult1> AddSampleBox(string name, string epc)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "SampleBox/AddSampleBox?name=" + name + "&epc=" + epc, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> DeleteSampleBox(string id)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "SampleBox/DeleteSampleBox?id=" + id, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        public static CommonResult<CommonResult1> EditSampleBox(string id, string name, string epc)
        {
            try
            {
                var flag = 0;
            ReStart:
                var result = GetRequest(url + "SampleBox/EditSampleBox?id=" + id + "&name=" + name + "&epc=" + epc, null, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }

        #endregion

        #region 放样/加急/送样，操作完成后的修改服务数据请求
        public static CommonResult<CommonResult1> EditBoxAndSampleBox(Comm com)
        {
            try
            {
                var flag = 0;
                var json = new JavaScriptSerializer().Serialize(com);
            ReStart:
                var result = GetRequest(url + "Common/EditBoxAndSampleBox", json, "POST", false);
                if (!result.result)
                {
                    if (flag < 1)
                    {
                        flag++;
                        goto ReStart;
                    }
                }
                if (!result.result)
                {
                    return new CommonResult<CommonResult1> { result = false, message = result.message };
                }
                var data = new JavaScriptSerializer().Deserialize<WebApiResponse<CommonResult1>>(result.data);
                if (data.Code != 200)
                {
                    return new CommonResult<CommonResult1>() { result = false, message = data.Message };
                }
                return new CommonResult<CommonResult1>() { result = true, data = data.Data, message = data.Message };
            }
            catch (Exception e)
            {
                return new CommonResult<CommonResult1>() { result = false, message = e.Message };
            }
        }


        #endregion





        public static CommonResult GetRequest(string url, string data = null, string method = "POST", bool Flag = true)
        {
            try
            {
                if (Flag)
                {
                    //LoginByUserName();
                }

                TextLogUtil.Info("请求url：" + url + "-----参数：" + (data ?? ""));
                string strBuff = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json; charset=utf-8";
                request.Method = method;
                request.ServicePoint.Expect100Continue = false;
                request.Timeout = 1000 * 10;
                request.ReadWriteTimeout = 1000 * 10;
                request.Headers.Add("Token", TokenStr);
                request.ContentLength = 0;
                if (!string.IsNullOrWhiteSpace(data))
                {
                    byte[] postdata = Encoding.UTF8.GetBytes(data);
                    request.ContentLength = postdata.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(postdata, 0, postdata.Length);
                    newStream.Close();
                }

                HttpWebResponse httpResp = (HttpWebResponse)request.GetResponse();
                Stream respStream = httpResp.GetResponseStream();
                StreamReader respStreamReader = new StreamReader(respStream, Encoding.UTF8);
                strBuff = respStreamReader.ReadToEnd();
                request.Abort();
                httpResp.Close();
                if (respStream != null)
                {
                    respStream.Close();
                }
                respStreamReader.Close();

                TextLogUtil.Info("返回：" + strBuff);
                return new CommonResult() { result = true, data = strBuff };
            }
            catch (Exception ex)
            {
                //TextLogUtil.Info(ex.Message);
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
    }
}
