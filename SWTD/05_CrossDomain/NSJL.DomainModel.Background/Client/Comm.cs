﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    //上传放样或取样给服务器修改的数据
    public class Comm
    {
        public int BoxNum { get; set; }
        public string CabName { get; set; }
        public string SampleBoxEpc { get; set; }
        public string State { get; set; }
        public int SampleNum { get; set; }

    }
}
