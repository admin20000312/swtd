﻿using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataAccess
{
    public class SJLDbContext : DbContext
    {
        public SJLDbContext()
            : base("server=127.0.0.1;database=TC1; user=root; password=123456;")
        {
            //"server=127.0.0.1;database=TC; user=root; password=123456;"
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            //modelBuilder.Conventions.Remove<IncludeMetadataConvention>();//移除对MetaData表的查询验证，要不然每次都要访问EdmMetadata这个表

            Database.SetInitializer<SJLDbContext>(null);
        }
        public DbSet<BoxInfo> BoxInfo { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<OpenBoxInfo> OpenBoxInfo { get; set; }
        public DbSet<AdminInfo> AdminInfo { get; set; }
        public DbSet<AdminLogInfo> AdminLogInfo { get; set; }
        public DbSet<CabinetInfo> CabinetInfo { get; set; }
        public DbSet<LogInfo> LogInfo { get; set; }

        //public DbSet<RFIDInfo> RFIDInfo { get; set; }
        public DbSet<GoodsClassInfo> GoodsClassInfo { get; set; }
        


    }
}
