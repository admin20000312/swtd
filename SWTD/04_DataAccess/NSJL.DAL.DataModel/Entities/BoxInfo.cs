﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class BoxInfo
    {
        [Key]
        public string Id { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //箱门大小   小   中   大
        public string BoxType { get; set; }
        //是否锁定   默认false
        public bool? IsLock { get; set; }
        //借出的用户ID  为null 空表示未借出
        public string UserId { get; set; }


        //清箱清理以下字段-------------------------
        //是否绑定物品 默认false
        //是否空箱   默认为true
        public bool? IsFree { get; set; }
        public bool? IsBind { get; set; }
        ////标签号
        //public string GoodsId { get; set; }
        //物资编号
        public string GoodsNum { get; set; }
        //物资分类
        public string GoodsClasss { get; set; }
        //绑定物品名称
        public string GoodsName { get; set; }
        //绑定物品时间
        public DateTime? BindTime { get; set; }
        //------------------------------------------


        //最后开门时间
        public DateTime? UpdateTime { get; set; }
        //绑定时间
        public DateTime? CreateTime { get; set; }
        //柜号名称
        public string CabName { get; set; }

    }
}