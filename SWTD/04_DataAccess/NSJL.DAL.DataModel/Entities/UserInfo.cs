﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class UserInfo
    {
        [Key]
        public string Id { get; set; }
        //姓名
        public string Name { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        //角色
        public string Type { get; set; } = null;
        public string Password { get; set; } = null;
        //头像 base64
        public string HeadPic { get; set; } = null;
        public string FaceId { get; set; } = null;
        //二维码
        public string QRCode { get; set; } = null;
        //卡号
        public string Code { get; set; }
        //指纹号
        public string FingNo { get; set; } = null;
        //指纹特征
        public string Features { get; set; }
        //是否管理员
        public bool? IsAdmin { get; set; }
        public string Mobile { get; set; }
        public string BoxNums { get; set; } = null;
        public string GoodsClasss { get; set; }
        //public string RFIDNum { get; set; }
    }
}
