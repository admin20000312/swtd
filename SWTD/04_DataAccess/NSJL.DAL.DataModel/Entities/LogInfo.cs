﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class LogInfo
    {
        [Key]
        public string Id { get; set; }
        public string Url { get; set; }
        //柜号名称
        public string CabName { get; set; }
        //状态
        public string Type { get; set; }
        //请求参数
        public string RequestBody { get; set; }
        //响应参数
        public string ResponseBody { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
