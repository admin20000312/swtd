﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class OpenBoxInfo
    {
        [Key]
        public string Id { get; set; }
        //姓名
        public string Name { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //操作类型   存   取
        public string Type { get; set; }
        //数量
        public int? Num { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        public string CabName { get; set; }
        //补库  领料  退料  取料  移库
        public string State { get; set; }



        //绑定物品名称
        public string GoodsName { get; set; }
        //卡号
        public string Code { get; set; }
        //指纹号
        public string FingNo { get; set; }
        //指纹编码
        public string Features { get; set; }

        public string GoodsId { get; set; }
        public string GoodsNum { get; set; }
        //人脸编码
        public string HeadPic { get; set; }

        //RFID标签号
        //public string RFIDNum { get; set; }
    }
}
