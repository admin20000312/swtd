﻿using System;
using System.Collections.Generic;

namespace NSJL.DAL.DataModel.Models
{
    public partial class SampleBoxInfo
    {
        public string Id { get; set; }
        /// <summary>
        /// 样品箱名称
        /// </summary>
        public string SampleBoxName { get; set; } = null;
        /// <summary>
        /// RFID标签号
        /// </summary>
        public string SampleBoxEpc { get; set; }= null;
        /// <summary>
        /// 样品袋数量：0表示没有放样品
        /// </summary>
        public int? SampleNum { get; set; }
    }
}
