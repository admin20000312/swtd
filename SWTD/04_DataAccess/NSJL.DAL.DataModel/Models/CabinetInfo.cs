﻿using System;
using System.Collections.Generic;

namespace NSJL.DAL.DataModel.Models
{
    public partial class CabinetInfo
    {
        public string Id { get; set; }
        public string Name { get; set; } = null;
        public DateTime? DateTime { get; set; } = null;
    }
}
