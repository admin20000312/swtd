﻿using swtdWin.Dialog;
using swtdWin.Interface;
using swtdWin.Utils;
using swtdWin.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace swtdWin
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 退出系统
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Button_Exit(object sender, RoutedEventArgs e)
        //{
        //    Environment.Exit(0);
        //}

        private void Button_Login(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string password = this.password.Password;
            var result = InterBiz.Login(name,password);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                Label1.Content = result.message;
                return;
            }
            Label1.Content = null;
            Main main = new Main(name);
            main.Show();
            this.Hide();
        }


        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        /// <summary>
        /// 链接跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = sender as Hyperlink;
            Process.Start(new ProcessStartInfo(link.NavigateUri.AbsoluteUri));
        }
    }
}
