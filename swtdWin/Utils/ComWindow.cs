﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace swtdWin.Utils
{
    public class ComWindow
    {
        public static void Focus(object sender)
        {
            if (sender.GetType().Name == "PasswordBox")
            {
                var pwd = (PasswordBox)sender;
                if (!string.IsNullOrWhiteSpace(pwd.Password))
                {
                    pwd.GetType().GetMethod("Select", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Invoke(pwd, new object[] { pwd.Password.Length, 0 });
                }
                pwd.Focus();
            }
            else
            {
                var txt = (TextBox)sender;
                txt.SelectionStart = (txt.Text ?? "").Length;
                txt.Focus();
            }
            SelfUtil.Open();//打开虚拟键盘
        }
    }  
}
