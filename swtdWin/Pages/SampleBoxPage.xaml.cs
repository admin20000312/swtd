﻿using swtdWin.Dialog;
using swtdWin.Interface;
using swtdWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace swtdWin.Pages
{
    /// <summary>
    /// SampleBoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class SampleBoxPage : Page
    {
        public SampleBoxPage()
        {
            InitializeComponent();
        }

        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            SampleBoxInfo info = (SampleBoxInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中行");
                return;
            }
            SampleBoxDel sbd = new SampleBoxDel();
            sbd.DeleteId = info.Id;
            sbd.name.Content = info.SampleBoxName;
            sbd.epc.Content = info.SampleBoxEpc;
            var result = sbd.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("删除成功");
            }
            var commonResult = InterBiz.GetAllSampleBox();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = commonResult.data;
        }


        //添加
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            SampleBoxEdit sbe = new SampleBoxEdit();
            sbe.labTip.Content = "添加";
            sbe.btn1.Content = "添加";
            var result = sbe.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("添加成功");
            }
            var commonResult = InterBiz.GetAllSampleBox();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = commonResult.data;
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var result = InterBiz.GetAllSampleBox();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            DataGrid1.ItemsSource = result.data;
        }


        //编辑
        private void Button_Edit(object sender, RoutedEventArgs e)
        {
            SampleBoxInfo info = (SampleBoxInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中行");
                return;
            }
            SampleBoxEdit sbe = new SampleBoxEdit();
            sbe.labTip.Content = "编辑";
            sbe.btn1.Content = "编辑";
            sbe.EditId = info.Id;
            sbe.epc.Text = info.SampleBoxEpc;
            sbe.name.Text = info.SampleBoxName;
            var result = sbe.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("编辑成功");

            }
            var commonResult = InterBiz.GetAllSampleBox();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = commonResult.data;
        }
    }
}
