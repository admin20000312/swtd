﻿using swtdWin.Dialog;
using swtdWin.Interface;
using swtdWin.Models;
using swtdWin.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace swtdWin.Pages
{
    /// <summary>
    /// BoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class BoxPage : Page
    {
        public BoxPage()
        {
            InitializeComponent();
        }
        List<BoxInfo> data {  get; set; }
        List<BoxControl> listBox = new List<BoxControl>();
        public void Init()
        {
            listBox.Clear();
            var result = InterBiz.GetAllBox();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            WrapPanel1.Children.Clear();
            data = result.data;
            List<BoxInfo> boxInfos = data.OrderBy(p=>p.CabNum).ToList();
            Dictionary<string, List<BoxInfo>> dictionary = boxInfos.GroupBy(r => r.CabNum).ToDictionary(d => d.Key, d => d.ToList());
            foreach (var i in dictionary)
            {
                string key = i.Key;
                List<BoxInfo> boxs = data.Where(p=>p.CabNum == key).OrderBy(p=>p.BoxNum).ToList();
                foreach (var item in boxs)
                {
                    BoxControl btn = new BoxControl();
                    btn.Width = 154;
                    btn.Height = 125;
                    btn.Margin = new Thickness(5, 5, 5, 5);
                    btn.Label1.Content = item.BoxNum;
                    btn.BoxNum = item.BoxNum;

                    if (item.IsLock == 1)
                    {
                        btn.Label3.Content = "已锁定";
                        btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                    }
                    else
                    {
                        btn.Label3.Content = "未锁定";
                    }
                    btn.Label2.Text = "箱柜号：" + item.CabNum.ToString();
                    btn.Label4.Text = "状态：" + item.State;
                    var res = InterBiz.GetAllSampleBox();
                    if (!res.result)
                    {
                        MessageDialog.ShowDialog(res.message);
                        return;
                    }
                    var sampleBoxInfo = res.data.Where(p => p.SampleBoxEpc == item.SampleBoxEpc).FirstOrDefault();
                    if (sampleBoxInfo != null)
                    {
                        btn.Label5.Content = "样品数量：" + (sampleBoxInfo.SampleNum ?? 0);
                    }
                    else
                    {
                        btn.Label5.Content = "样品数量：0";
                    }
                    btn.MouseDown += MouseButtonEventHandler;
                    WrapPanel1.Children.Add(btn);
                }
            }
            
        }


        

        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;

                var filepath = "pack://application:,,,/Image/a未选中.png";

                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");

                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                listBox.Remove(u);
            }
            else
            {
                u.IsCheck = true;

                var filepath = "pack://application:,,,/Image/a选中.png";

                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");

                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label5.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                listBox.Add(u);
            }
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            comBox.Items.Clear();
            comBox.Items.Add("箱柜名称");
            comBox.SelectedIndex = 0;
            var list = InterBiz.GetAllCabinet().data.OrderBy(p=>p.Name).ToList();
            foreach (var item in list)
            {
                comBox.Items.Add(item.Name);
            }
            Init();
        }

        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        //初始化箱柜
        private void Button_InitBox(object sender, RoutedEventArgs e)
        {
            string cabName = comBox.Text;
            string num = tbNum.Text;
            if (cabName == "箱柜名称")
            {
                MessageDialog.ShowDialog("请选择初始化的箱柜");
                return;
            }
            if (string.IsNullOrWhiteSpace(num))
            {
                MessageDialog.ShowDialog("箱门数量不能为空");
                return;
            }
            if (!int.TryParse(num,out int Num))
            {
                MessageDialog.ShowDialog("请输入正确箱门数量");
                return;
            }
            var res = InterBiz.InitBox(cabName, Num);
            if (!res.result)
            {
                MessageDialog.ShowDialog(res.message);
                return;
            }
            MessageDialog.ShowDialog("添加成功");
            Init();
        }



        List<DelBox> listDel = new List<DelBox>();
        //批量删除
        private void Button_DeleteBox(object sender, RoutedEventArgs e)
        {
            if (listBox.Count <= 0)
            {
                MessageDialog.ShowDialog("未选中箱门");
                return;
            }
            listDel.Clear();
            foreach (var item in listBox)
            {
                DelBox delBox = new DelBox();
                delBox.BoxNum = item.BoxNum;
                string[] strings = item.Label2.Text.Split('：');
                delBox.CabNum = strings[1];
                listDel.Add(delBox);
            }
            var res = InterBiz.DeleteBox(listDel);
            if (!res.result)
            {
                MessageDialog.ShowDialog(res.message);
                return;
            }
            MessageDialog.ShowDialog("删除成功");
            Init();
        }

        private void Button_Refresh(object sender, RoutedEventArgs e)
        {
            Init();
        }
    }


    public class DelBox
    {
        public int BoxNum { get; set; }
        public string CabNum { get; set; }
    }
}

