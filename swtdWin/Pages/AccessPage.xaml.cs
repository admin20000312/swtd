﻿using swtdWin.Dialog;
using swtdWin.Interface;
using swtdWin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace swtdWin.Pages
{
    /// <summary>
    /// AccessPage.xaml 的交互逻辑
    /// </summary>
    public partial class AccessPage : Page
    {
        public AccessPage()
        {
            InitializeComponent();
        }


        //清除
        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            DatePicker1.Text = null;
            DatePicker1_Copy.Text = null;
        }

        //查询
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<AccessLog> list1 = list;
            var time = DatePicker1.Text;
            var endtime = DatePicker1_Copy.Text;
            DateTime? start = null;
            DateTime? end = null;
            if (!string.IsNullOrWhiteSpace(time))
            {
                start = Convert.ToDateTime(time);
                list1.Where(p => p.InDateTime > start);
            }
            if (!string.IsNullOrWhiteSpace(endtime))
            {
                end = Convert.ToDateTime(Convert.ToDateTime(endtime).ToString("yyyy-MM-dd 23:59:59"));
                list1.Where(p => p.OutDateTime < end);
            }
            DataGrid1.ItemsSource = list;
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var result = InterBiz.GetAllAccess();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            list = result.data;
            DataGrid1.ItemsSource = list;
        }

        List<AccessLog> list {  get; set; }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var result = InterBiz.GetAllAccess();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            list = result.data;
            DataGrid1.ItemsSource = list;
        }
    }
}
