﻿using swtdWin.Dialog;
using swtdWin.Interface;
using swtdWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace swtdWin.Pages
{
    /// <summary>
    /// CabinetPage.xaml 的交互逻辑
    /// </summary>
    public partial class CabinetPage : Page
    {
        public CabinetPage()
        {
            InitializeComponent();
            var result = InterBiz.GetAllCabinet();
            List<CabinetInfo> list = result.data.OrderByDescending(x => x.Name).ToList();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }

        //删除柜号
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            CabinetInfo info = (CabinetInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中行");
                return;
            }

            CabinetDel del = new CabinetDel();
            del.name.Content = info.Name;
            del.DeleteId = info.Id;
            var result = del.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("删除成功");
            }
            var commonResult = InterBiz.GetAllCabinet();
            List<CabinetInfo> list = commonResult.data.OrderByDescending(x => x.Name).ToList();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }
        //添加柜号
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            CabinetEdit del = new CabinetEdit();
            del.lbTip.Content = "添加";
            del.btn1.Content = "添加";
            var result = del.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("添加成功");
                
            }
            var commonResult = InterBiz.GetAllCabinet();
            List<CabinetInfo> list = commonResult.data.OrderByDescending(x => x.Name).ToList();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }

        //编辑
        private void Button_Edit(object sender, RoutedEventArgs e)
        {
            CabinetInfo info = (CabinetInfo)DataGrid1.SelectedItem;
            if (info == null)
            {
                MessageDialog.ShowDialog("未选中行");
                return;
            }
            CabinetEdit del = new CabinetEdit();
            del.name.Text = info.Name; 
            del.EditId = info.Id;
            del.lbTip.Content = "编辑";
            del.btn1.Content = "编辑";
            var result = del.ShowDialog();
            if (result == true)
            {
                MessageDialog.ShowDialog("编辑成功");

            }
            var commonResult = InterBiz.GetAllCabinet();
            List<CabinetInfo> list = commonResult.data.OrderByDescending(x => x.Name).ToList();
            DataGrid1.ItemsSource = null;
            DataGrid1.ItemsSource = list;
        }
    }
}
