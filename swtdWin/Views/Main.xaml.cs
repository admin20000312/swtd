﻿using swtdWin.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Button = System.Windows.Controls.Button;

namespace swtdWin.Views
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main : Window
    {
        public string UserName { get; set; }
        public Main(string name = null)
        {
            InitializeComponent();
            //登录修改用户名
            Label1.Content = name;
            //初始化page
            Button1_Click(Button1, null);
            //page5.action = (() =>
            //{
            //    page1.Init();
            //    //page8.Init();
            //});
        }

        public CabinetPage page1 = new CabinetPage();
        public BoxPage page2 = new BoxPage();
        public AccessPage page3 = new AccessPage();
        public SampleBoxPage page4 = new SampleBoxPage();



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
            Login login = new Login();
            login.Show();
        }

        //箱柜管理
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page1;
        }

        //箱格管理
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page2;
        }

        //存取记录
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page3;
        }


        //样品箱管理
        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page4;
        }


        public void SetBorder(object sender)
        {
            Button1.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button1.BorderThickness = new Thickness(1, 1, 1, 1);

            Button2.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button2.BorderThickness = new Thickness(1, 1, 1, 1);

            Button3.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button3.BorderThickness = new Thickness(1, 1, 1, 1);

            Button4.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button4.BorderThickness = new Thickness(1, 1, 1, 1);

            ((Button)sender).Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            ((Button)sender).BorderThickness = new Thickness();
        }
    }
}
