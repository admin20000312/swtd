﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace swtdWin.Model
{
    public class WebApiResponse
    {
        /// <summary>
        /// 错误码：0-成功，非0即失败
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 错误说明
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object Data { get; set; }
    }
    public class WebApiResponse<T>
    {
        /// <summary>
        /// 错误码：0-成功，非0即失败
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 错误说明
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }
    }
}
