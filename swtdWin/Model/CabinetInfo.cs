﻿using System;
using System.Collections.Generic;

namespace swtdWin.Models
{
    public partial class CabinetInfo
    {
        public string Id { get; set; }
        public string Name { get; set; } = null;
        public DateTime? DateTime { get; set; } = null;
    }
}
