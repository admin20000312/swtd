﻿using System;
using System.Collections.Generic;

namespace swtdWin.Model
{
    public partial class AccessLog
    {
        public string Id { get; set; }
        /// <summary>
        /// 开箱操作箱门号
        /// </summary>
        public int BoxNum { get; set; }
        /// <summary>
        /// 放样时间
        /// </summary>
        public DateTime? InDateTime { get; set; } = null;
        /// <summary>
        /// 取样时间
        /// </summary>
        public DateTime? OutDateTime { get; set; } = null;
        /// <summary>
        /// 样品箱标签号
        /// </summary>
        public string SampleBoxEpc { get; set; } = null;
        public string CabNum { get; set; } = null;
    }
}
