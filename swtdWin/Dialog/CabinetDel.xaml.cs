﻿using swtdWin.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace swtdWin.Dialog
{
    /// <summary>
    /// CabinetDel.xaml 的交互逻辑
    /// </summary>
    public partial class CabinetDel : Window
    {
        public CabinetDel()
        {
            InitializeComponent();
        }

        public string DeleteId  { get; set; }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            var result = InterBiz.DeleteCabinet(DeleteId);
            if (result.result)
            {
                this.DialogResult = true;
            }
            else
            {
                MessageDialog.ShowDialog(result.message);
                this.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }
    }
}
