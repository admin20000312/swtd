﻿using swtdWin.Interface;
using swtdWin.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace swtdWin.Dialog
{
    /// <summary>
    /// SampleBoxEdit.xaml 的交互逻辑
    /// </summary>
    public partial class SampleBoxEdit : Window
    {
        public SampleBoxEdit()
        {
            InitializeComponent();
            Task.Run(() =>
            {
                try
                {
                    var ini = new IniFile(@"\Config\Config.ini");
                    var isOpen = ini.readKey("LabelConfig", "isOpen");
                    if (isOpen == "1")
                    {
                        var portName = ini.readKey("LabelConfig", "portName");
                        var baudRate = Convert.ToInt32(ini.readKey("LabelConfig", "baudRate"));
                        RFIDHelper.GetInstance().ConnectCOM(portName, baudRate);
                        RFIDHelper.GetInstance().CallBack = (a, b) => {
                            Dispatcher.Invoke(() =>
                            {
                                epc.Text = a;
                            });
                        };
                        RFIDHelper.GetInstance().StartRead();
                    }
                }
                catch (Exception e)
                {
                    MessageDialog.ShowDialog(e.Message);
                    return;
                }
            });
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RFIDHelper.GetInstance().Close();
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        public string EditId { get; set; } = null;
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(EditId))//添加
            {
                var result = InterBiz.AddSampleBox(name.Text,epc.Text);
                if (result.result)
                {
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageDialog.ShowDialog(result.message);
                }
            }
            else //编辑
            {
                var result = InterBiz.EditSampleBox(EditId, name.Text,epc.Text);
                if (result.result)
                {
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageDialog.ShowDialog(result.message);
                }
            }
        }
    }
}
