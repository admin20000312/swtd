﻿using swtdWin.Interface;
using swtdWin.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace swtdWin.Dialog
{
    /// <summary>
    /// CabinetEdit.xaml 的交互逻辑
    /// </summary>
    public partial class CabinetEdit : Window
    {
        public CabinetEdit()
        {
            InitializeComponent();
        }

        public string EditId { get; set; } = null;

        
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(EditId)) //添加
            {
                var result = InterBiz.AddCabinet(name.Text);
                if (result.result)
                {
                    this.DialogResult = true;
                    this.Close();
                }
                else 
                {
                    MessageDialog.ShowDialog(result.message);
                }
            }
            else  // 编辑
            {
                var result = InterBiz.EditCabinet(EditId,name.Text);
                if (result.result)
                {
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageDialog.ShowDialog(result.message);
                }
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            EditId = null;
        }
    }
}
