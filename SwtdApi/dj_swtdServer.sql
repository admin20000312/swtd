/*
 Navicat Premium Data Transfer

 Source Server         : server
 Source Server Type    : SQL Server
 Source Server Version : 16001000
 Source Catalog        : dj_swtdServer
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 16001000
 File Encoding         : 65001

 Date: 29/12/2023 17:29:46
*/


-- ----------------------------
-- Table structure for AccessLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessLog]') AND type IN ('U'))
	DROP TABLE [dbo].[AccessLog]
GO

CREATE TABLE [dbo].[AccessLog] (
  [Id] varchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [BoxNum] int  NULL,
  [InDateTime] datetime  NULL,
  [OutDateTime] datetime  NULL,
  [SampleBoxEpc] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[AccessLog] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'开箱操作箱门号',
'SCHEMA', N'dbo',
'TABLE', N'AccessLog',
'COLUMN', N'BoxNum'
GO

EXEC sp_addextendedproperty
'MS_Description', N'放样时间',
'SCHEMA', N'dbo',
'TABLE', N'AccessLog',
'COLUMN', N'InDateTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'取样时间',
'SCHEMA', N'dbo',
'TABLE', N'AccessLog',
'COLUMN', N'OutDateTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'样品箱标签号',
'SCHEMA', N'dbo',
'TABLE', N'AccessLog',
'COLUMN', N'SampleBoxEpc'
GO


-- ----------------------------
-- Records of AccessLog
-- ----------------------------
INSERT INTO [dbo].[AccessLog] ([Id], [BoxNum], [InDateTime], [OutDateTime], [SampleBoxEpc]) VALUES (N'282fa9574814490889ea60564cabf70d', N'1', N'2023-12-29 13:34:44.000', NULL, N'Epc1265674897812')
GO


-- ----------------------------
-- Table structure for BoxInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[BoxInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[BoxInfo]
GO

CREATE TABLE [dbo].[BoxInfo] (
  [Id] varchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [BoxNum] int  NULL,
  [IsLock] tinyint  NULL,
  [CabNum] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [State] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [UpdateTime] datetime  NULL,
  [CreateTime] datetime  NULL,
  [SampleBoxEpc] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[BoxInfo] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'箱门号',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'BoxNum'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否锁箱子',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'IsLock'
GO

EXEC sp_addextendedproperty
'MS_Description', N'箱柜号',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'CabNum'
GO

EXEC sp_addextendedproperty
'MS_Description', N'状态：在库、空闲',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'State'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'UpdateTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'CreateTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'样品箱标签号：有值表示箱门有物，否则就是空箱',
'SCHEMA', N'dbo',
'TABLE', N'BoxInfo',
'COLUMN', N'SampleBoxEpc'
GO


-- ----------------------------
-- Records of BoxInfo
-- ----------------------------
INSERT INTO [dbo].[BoxInfo] ([Id], [BoxNum], [IsLock], [CabNum], [State], [UpdateTime], [CreateTime], [SampleBoxEpc]) VALUES (N'18fcad04b9384400b7962888cdbe499f', N'1', N'0', N'001', N'在库', N'2023-12-29 11:31:53.000', N'2023-12-29 11:31:55.000', N'Epc1265674897812')
GO


-- ----------------------------
-- Table structure for CabinetInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[CabinetInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[CabinetInfo]
GO

CREATE TABLE [dbo].[CabinetInfo] (
  [Id] varchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Name] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [DateTime] datetime2(7)  NULL
)
GO

ALTER TABLE [dbo].[CabinetInfo] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of CabinetInfo
-- ----------------------------
INSERT INTO [dbo].[CabinetInfo] ([Id], [Name], [DateTime]) VALUES (N'085a309802f945ad8373114e413743b2', N'001', N'2023-12-29 11:17:24.2169034')
GO


-- ----------------------------
-- Table structure for SampleBoxInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SampleBoxInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[SampleBoxInfo]
GO

CREATE TABLE [dbo].[SampleBoxInfo] (
  [Id] varchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [SampleBoxName] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [SampleBoxEpc] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [SampleNum] int  NULL
)
GO

ALTER TABLE [dbo].[SampleBoxInfo] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'样品箱名称',
'SCHEMA', N'dbo',
'TABLE', N'SampleBoxInfo',
'COLUMN', N'SampleBoxName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'RFID标签号',
'SCHEMA', N'dbo',
'TABLE', N'SampleBoxInfo',
'COLUMN', N'SampleBoxEpc'
GO

EXEC sp_addextendedproperty
'MS_Description', N'样品袋数量：0表示没有放样品',
'SCHEMA', N'dbo',
'TABLE', N'SampleBoxInfo',
'COLUMN', N'SampleNum'
GO


-- ----------------------------
-- Records of SampleBoxInfo
-- ----------------------------
INSERT INTO [dbo].[SampleBoxInfo] ([Id], [SampleBoxName], [SampleBoxEpc], [SampleNum]) VALUES (N'5591dfcf52d74b60ab580a25553f4abe', N'样品箱002', N'Epc100000', NULL)
GO

INSERT INTO [dbo].[SampleBoxInfo] ([Id], [SampleBoxName], [SampleBoxEpc], [SampleNum]) VALUES (N'a07593bb8cf547549c2f1101a251fd41', N'样品箱001', N'Epc1265674897812', N'20')
GO


-- ----------------------------
-- Table structure for UserLogin
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[UserLogin]') AND type IN ('U'))
	DROP TABLE [dbo].[UserLogin]
GO

CREATE TABLE [dbo].[UserLogin] (
  [Id] varchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [UserName] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [Password] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[UserLogin] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'UserLogin',
'COLUMN', N'UserName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录密码',
'SCHEMA', N'dbo',
'TABLE', N'UserLogin',
'COLUMN', N'Password'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'UserLogin',
'COLUMN', N'CreateTime'
GO


-- ----------------------------
-- Records of UserLogin
-- ----------------------------
INSERT INTO [dbo].[UserLogin] ([Id], [UserName], [Password], [CreateTime]) VALUES (N'7b037707c5934d5e8d239775ed086ca1', N'admin', N'123456', N'2023-12-29 14:02:14.000')
GO


-- ----------------------------
-- Primary Key structure for table AccessLog
-- ----------------------------
ALTER TABLE [dbo].[AccessLog] ADD CONSTRAINT [PK__AccessLo__3214EC07EEC815EA] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table BoxInfo
-- ----------------------------
ALTER TABLE [dbo].[BoxInfo] ADD CONSTRAINT [PK__BoxInfo__3214EC0719A700B8] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table CabinetInfo
-- ----------------------------
ALTER TABLE [dbo].[CabinetInfo] ADD CONSTRAINT [PK__CabinetI__3214EC0776AEEEE5] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SampleBoxInfo
-- ----------------------------
ALTER TABLE [dbo].[SampleBoxInfo] ADD CONSTRAINT [PK__SampleBo__3214EC076D51E326] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table UserLogin
-- ----------------------------
ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [PK__UserLogi__3214EC07321DA994] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

