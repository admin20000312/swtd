﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json.Linq;
using SwtdApi.Utils;
using System.Diagnostics;

namespace SwtdApi.Common
{
    /// 登陆拦截器
    /// </summary>
    public class LoginFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 在请求接口方法之前拦截
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                //////获取请求携带的参数的内容
                ////object? account = context.ActionArguments["account"];
                ////object? pwd = context.ActionArguments["pwd"];
                ////获取请求头的内容
                //var token = context.HttpContext.Request.Headers["Token"];
                //RedisHelper redisHelper = new RedisHelper();
                //string tok = redisHelper.StringGet("Token");
                //if (string.IsNullOrWhiteSpace(tok) || (tok != token))
                //{
                //    context.Result = new JsonResult("401  还没未登录授权或权限认证过期，请先登录！");
                //}

            }
            catch (Exception ex)
            {
                context.Result = new JsonResult("服务异常："+ex.Message);
            }
        }


        /// <summary>
        /// 在请求接口方法之后拦截
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            //Debug.WriteLine("在请求接口方法之后拦截");
        }
    }
}
