﻿namespace SwtdApi.Common
{

    /// <summary>
    /// web api（接口）的返回结果
    /// </summary>
    public class WebApiResponse
    {

        /// <summary>
        /// 错误码：0-成功，非0即失败
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 错误说明
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 创建一个实例
        /// </summary>
        public static WebApiResponse Create(int code, string message, object data)
        {
            WebApiResponse response = new WebApiResponse();
            response.Code = code;
            response.Message = message;
            response.Data = data;
            return response;
        }

        /// <summary>
        /// 创建一个实例
        /// </summary>
        public static WebApiResponse Create(int code, string message)
        {
            return Create(code, message, null);
        }

        /// <summary>
        /// 创建一个成功的实例
        /// </summary>
        public static WebApiResponse OK(object data)
        {
            return Create(200, "OK", data);
        }

        /// <summary>
        /// 创建一个成功的实例
        /// </summary>
        public static WebApiResponse OK()
        {
            return OK(null);
        }


    }
}
