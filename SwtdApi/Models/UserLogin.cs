﻿using System;
using System.Collections.Generic;

namespace SwtdApi.Models
{
    public partial class UserLogin
    {
        public string Id { get; set; } = null!;
        /// <summary>
        /// 用户名
        /// </summary>
        public string? UserName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string? Password { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}
