﻿using System;
using System.Collections.Generic;

namespace SwtdApi.Models
{
    public partial class BoxInfo
    {
        public string Id { get; set; } = null!;
        /// <summary>
        /// 箱门号
        /// </summary>
        public int? BoxNum { get; set; }
        /// <summary>
        /// 是否锁箱子
        /// </summary>
        public byte? IsLock { get; set; }
        /// <summary>
        /// 箱柜号
        /// </summary>
        public string? CabNum { get; set; }
        /// <summary>
        /// 状态：在库、空闲、加急
        /// </summary>
        public string? State { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 样品箱标签号：有值表示箱门有物，否则就是空箱
        /// </summary>
        public string? SampleBoxEpc { get; set; }
    }
}
