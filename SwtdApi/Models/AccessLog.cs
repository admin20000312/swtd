﻿using System;
using System.Collections.Generic;

namespace SwtdApi.Models
{
    public partial class AccessLog
    {
        public string Id { get; set; } = null!;
        /// <summary>
        /// 开箱操作箱门号
        /// </summary>
        public int? BoxNum { get; set; }
        /// <summary>
        /// 放样时间
        /// </summary>
        public DateTime? InDateTime { get; set; }
        /// <summary>
        /// 取样时间
        /// </summary>
        public DateTime? OutDateTime { get; set; }
        /// <summary>
        /// 样品箱标签号
        /// </summary>
        public string? SampleBoxEpc { get; set; }
        /// <summary>
        /// 箱柜编号
        /// </summary>
        public string? CabNum { get; set; }
    }
}
