﻿using System;
using System.Collections.Generic;

namespace SwtdApi.Models
{
    public partial class CabinetInfo
    {
        public string Id { get; set; } = null!;
        public string? Name { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
