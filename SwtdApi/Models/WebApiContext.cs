﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SwtdApi.Models
{
    public partial class WebApiContext : DbContext
    {
        public WebApiContext()
        {
        }

        public WebApiContext(DbContextOptions<WebApiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessLog> AccessLogs { get; set; } = null!;
        public virtual DbSet<BoxInfo> BoxInfos { get; set; } = null!;
        public virtual DbSet<CabinetInfo> CabinetInfos { get; set; } = null!;
        public virtual DbSet<SampleBoxInfo> SampleBoxInfos { get; set; } = null!;
        public virtual DbSet<UserLogin> UserLogins { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.;Database=dj_swtdServer;uid=sa;Password=123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessLog>(entity =>
            {
                entity.ToTable("AccessLog");

                entity.Property(e => e.Id)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BoxNum).HasComment("开箱操作箱门号");

                entity.Property(e => e.CabNum)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("箱柜编号");

                entity.Property(e => e.InDateTime)
                    .HasColumnType("datetime")
                    .HasComment("放样时间");

                entity.Property(e => e.OutDateTime)
                    .HasColumnType("datetime")
                    .HasComment("取样时间");

                entity.Property(e => e.SampleBoxEpc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("样品箱标签号");
            });

            modelBuilder.Entity<BoxInfo>(entity =>
            {
                entity.ToTable("BoxInfo");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BoxNum).HasComment("箱门号");

                entity.Property(e => e.CabNum)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("箱柜号");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasComment("创建时间");

                entity.Property(e => e.IsLock).HasComment("是否锁箱子");

                entity.Property(e => e.SampleBoxEpc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("样品箱标签号：有值表示箱门有物，否则就是空箱");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("状态：在库、空闲、加急");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("datetime")
                    .HasComment("更新时间");
            });

            modelBuilder.Entity<CabinetInfo>(entity =>
            {
                entity.ToTable("CabinetInfo");

                entity.Property(e => e.Id)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SampleBoxInfo>(entity =>
            {
                entity.ToTable("SampleBoxInfo");

                entity.Property(e => e.Id)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SampleBoxEpc)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("RFID标签号");

                entity.Property(e => e.SampleBoxName)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("样品箱名称");

                entity.Property(e => e.SampleNum).HasComment("样品袋数量：0表示没有放样品");
            });

            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.ToTable("UserLogin");

                entity.Property(e => e.Id)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasComment("创建时间");

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("登录密码");

                entity.Property(e => e.UserName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("用户名");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
