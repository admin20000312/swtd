﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Models;

namespace SwtdApi.Manage
{
    public class AccessLogManage
    {
        public static WebApiResponse SelectAll()
        {
            WebApiContext context = new WebApiContext();
            List<AccessLog> list = context.AccessLogs.ToList();
            return WebApiResponse.OK(list);
        }


        public static WebApiResponse AddAccessLog(int type,AccessLog al)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                if (al != null)
                {
                    if (type == 0) //0 ：放样/加急  1：送样
                    {
                        al.InDateTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                    }
                    else
                    {
                        al.OutDateTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                    }
                    context.AccessLogs.Add(al);
                    context.SaveChanges();
                }
                return WebApiResponse.Create(200, "操作记录添加成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
    }
}
