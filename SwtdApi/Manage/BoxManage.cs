﻿using Nancy.Json;
using SwtdApi.Common;
using SwtdApi.Models;

namespace SwtdApi.Manage
{
    public class BoxManage
    {
        public static WebApiResponse SelectAll()
        {
            WebApiContext context = new WebApiContext();
            List<BoxInfo> list = context.BoxInfos.ToList();
            return WebApiResponse.OK(list);
        }
        
        
        public static WebApiResponse SelectBoxByCab(string cabName)
        {
            WebApiContext context = new WebApiContext();
            List<BoxInfo> list = context.BoxInfos.Where(p=>p.CabNum == cabName).ToList();
            return WebApiResponse.OK(list);
        }

        //初始化箱柜的箱格数据
        public static WebApiResponse InitBox(string cabNum, int num)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                List<BoxInfo> boxInfos = context.BoxInfos.Where(p => p.CabNum == cabNum).ToList();
                List<BoxInfo> boxs = boxInfos.Where(p=>p.State != "空闲").ToList();
                if(boxs.Count > 0)
                {
                    return WebApiResponse.Create(401, "初始化箱柜数据失败（部分箱格不是空闲状态）");
                }
                foreach(BoxInfo box in boxInfos)
                {
                    context.BoxInfos.Remove(box);
                }
                for (int i = 1;i<= num;i++)
                {
                    BoxInfo bi = new BoxInfo();
                    bi.Id = Guid.NewGuid().ToString("N");
                    bi.BoxNum = i;
                    bi.CabNum = cabNum;
                    bi.State = "空闲";
                    bi.IsLock = 0;
                    bi.CreateTime = DateTime.Now;
                    bi.UpdateTime = DateTime.Now;
                    context.BoxInfos.Add(bi);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "初始化成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
        
        //编辑箱门信息
        public static WebApiResponse EditBox(BoxInfo box)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                BoxInfo boxInfos = context.BoxInfos.Where(p => p.Id == box.Id).FirstOrDefault();
                if (boxInfos != null)
                {
                    boxInfos.IsLock = box.IsLock;
                    boxInfos.SampleBoxEpc = box.SampleBoxEpc;
                    boxInfos.UpdateTime = DateTime.Now;
                    boxInfos.State = box.State;
                    context.BoxInfos.Update(boxInfos);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "箱门数据修改成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }


        //批量删除箱门
        public static WebApiResponse DeleteBox(string json)
        {
            try
            {
                var list = new JavaScriptSerializer().Deserialize<List<DelBox>>(json);
                WebApiContext context = new WebApiContext();
                foreach (var item in list)
                {
                    BoxInfo? box = context.BoxInfos.Where(p => p.CabNum == item.CabNum && p.BoxNum == item.BoxNum).FirstOrDefault();
                    if (box != null && !string.IsNullOrWhiteSpace(box.SampleBoxEpc))
                    {
                        return WebApiResponse.Create(401, "箱门删除失败，存在箱门不是空闲状态");
                    }
                    context.BoxInfos.Remove(box);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "箱门删除成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
    }

    public class DelBox
    {
        public int BoxNum { get; set; }
        public string CabNum { get; set; }
    }
}
