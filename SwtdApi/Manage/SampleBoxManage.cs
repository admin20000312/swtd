﻿using SwtdApi.Common;
using SwtdApi.Models;

namespace SwtdApi.Manage
{
    public class SampleBoxManage
    {
        public static WebApiResponse SelectAll()
        {
            WebApiContext context = new WebApiContext();
            List<SampleBoxInfo> list = context.SampleBoxInfos.ToList();
            return WebApiResponse.OK(list);
        }
        
        
        public static WebApiResponse AddSampleBox(string name,string epc)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                List<SampleBoxInfo> list = context.SampleBoxInfos.Where(p => (p.SampleBoxName == name || p.SampleBoxEpc == epc)).ToList();
                if (list.Count > 0)
                {
                    return WebApiResponse.Create(412, "添加失败，不满足添加条件（样品箱名或者标签号已存在）");
                }
                SampleBoxInfo samBox = new SampleBoxInfo();
                samBox.Id = Guid.NewGuid().ToString("N");
                samBox.SampleBoxName = name;
                if (!string.IsNullOrWhiteSpace(epc))
                {
                    samBox.SampleBoxEpc = epc;
                }
                context.SampleBoxInfos.Add(samBox);
                context.SaveChanges();
                return WebApiResponse.Create(200, "添加成功");
            }catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
        
        public static WebApiResponse EditSampleBox(string id, string name, string epc)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                var sampleBoxInfos = context.SampleBoxInfos.Where(p => p.Id == id).FirstOrDefault();
                string? oldEpc = sampleBoxInfos.SampleBoxEpc;
                if (sampleBoxInfos == null)
                {
                    return WebApiResponse.Create(204, "修改失败，样品箱不存在");
                }
                var list = context.SampleBoxInfos.Where(p => p.Id != id && (p.SampleBoxName == name || p.SampleBoxEpc == epc)).ToList();
                if (list.Count > 0)
                {
                    return WebApiResponse.Create(412, "修改失败，不满足修改条件（样品箱名或者标签号已存在）");
                }
                sampleBoxInfos.SampleBoxEpc = epc;
                sampleBoxInfos.SampleBoxName = name;
                context.SampleBoxInfos.Update(sampleBoxInfos);
                List<BoxInfo> boxInfos = context.BoxInfos.Where(p=>p.SampleBoxEpc == oldEpc).ToList();
                foreach (BoxInfo boxInfo in boxInfos)
                {
                    boxInfo.SampleBoxEpc = epc;
                    context.BoxInfos.Update(boxInfo);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "修改成功");
            }catch(Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
        
        public static WebApiResponse DeleteSampleBox(string id)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                var cabInfo = context.SampleBoxInfos.Where(p => p.Id == id).FirstOrDefault();
                string? oldEpc = cabInfo.SampleBoxEpc;
                if (cabInfo == null)
                {
                    return WebApiResponse.Create(204, "删除失败，样品箱不存在");
                }
                List<BoxInfo> boxInfos = context.BoxInfos.Where(p => p.SampleBoxEpc == oldEpc).ToList();
                if (boxInfos.Count > 0)
                {
                    return WebApiResponse.Create(304, "删除是失败，样品箱在使用");
                }
                context.SampleBoxInfos.Remove(cabInfo);
                context.SaveChanges();
                return WebApiResponse.Create(200, "删除成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
    }
}
