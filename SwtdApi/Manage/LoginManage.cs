﻿using SwtdApi.Common;
using SwtdApi.Models;
using SwtdApi.Utils;

namespace SwtdApi.Manage
{
    public class LoginManage
    {
        public static WebApiResponse Login(string name, string pwd)
        {
            WebApiContext context = new WebApiContext();
            var user = context.UserLogins.Where(p=>p.UserName == name && p.Password == pwd).FirstOrDefault();
            if (user == null)
            {
                return WebApiResponse.Create(401,"账号或密码不正确");
            }
            string token = Guid.NewGuid().ToString("N");
            //RedisHelper redis = new RedisHelper();
            //redis.StringSet("Token", token,TimeSpan.FromHours(24));
            return WebApiResponse.OK(token);
        }
    }
}
