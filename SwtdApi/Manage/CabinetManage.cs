﻿using SwtdApi.Common;
using SwtdApi.Models;
using System;

namespace SwtdApi.Manage
{
    public class CabinetManage
    {
        public static WebApiResponse SelectAll()
        {
            WebApiContext context = new WebApiContext();
            List<CabinetInfo> list = context.CabinetInfos.ToList();
            return WebApiResponse.OK(list);
        }
        
        
        public static WebApiResponse AddCabinet(string name)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                List<CabinetInfo> cabinetInfos = context.CabinetInfos.Where(p => p.Name == name).ToList();
                if (cabinetInfos.Count > 0)
                {
                    return WebApiResponse.Create(412, "添加失败，不满足添加条件（箱柜名已存在）");
                }
                CabinetInfo cab = new CabinetInfo();
                cab.Name = name;
                cab.Id = Guid.NewGuid().ToString("N");
                cab.DateTime = DateTime.Now;
                context.CabinetInfos.Add(cab);
                context.SaveChanges();
                return WebApiResponse.Create(200, "添加成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500,ex.Message);
            }
        }
        
        public static WebApiResponse EditCabinet(string id,string name)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                var cabInfo = context.CabinetInfos.Where(p => p.Id == id).FirstOrDefault();
                string? oldName = cabInfo.Name;
                if (cabInfo == null)
                {
                    return WebApiResponse.Create(204, "修改失败，箱柜编号不存在");
                }
                List<CabinetInfo> cabinetInfos = context.CabinetInfos.Where (p => p.Id != id && p.Name == name).ToList();
                if (cabinetInfos.Count > 0)
                {
                    return WebApiResponse.Create(412, "修改失败，不满足修改条件（箱柜名已存在）");
                }
                cabInfo.Name = name;
                cabInfo.DateTime = DateTime.Now;
                context.CabinetInfos.Update(cabInfo);
                List<BoxInfo> boxInfos = context.BoxInfos.Where(p=>p.CabNum == oldName).ToList();
                foreach (BoxInfo boxInfo in boxInfos)
                {
                    boxInfo.CabNum = name;
                    context.BoxInfos.Update(boxInfo);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "修改成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500,ex.Message);
            }
        }  
        

        public static WebApiResponse DeleteCabinet(string id)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                var cabInfo = context.CabinetInfos.Where(p => p.Id == id).FirstOrDefault();
                if (cabInfo == null)
                {
                    return WebApiResponse.Create(204, "删除失败，箱柜不存在");
                }
                List<BoxInfo> boxInfos = context.BoxInfos.Where(p=>p.CabNum == cabInfo.Name).ToList();
                if (boxInfos.Count > 0)
                {
                    return WebApiResponse.Create(304, "删除是失败，请先解绑箱格");
                }
                context.CabinetInfos.Remove(cabInfo);
                context.SaveChanges();
                return WebApiResponse.Create(200, "删除成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500,ex.Message);
            }
        }
    }
}
