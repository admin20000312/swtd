﻿using SwtdApi.Common;
using SwtdApi.Controllers;
using SwtdApi.Models;

namespace SwtdApi.Manage
{
    public class CommonManage
    {
        //编辑箱门和样品箱信息
        public static WebApiResponse EditBoxAndSample(Comm com)
        {
            try
            {
                WebApiContext context = new WebApiContext();
                BoxInfo boxInfos = context.BoxInfos.Where(p => p.BoxNum == com.BoxNum && p.CabNum == com.CabName).FirstOrDefault();
                if (boxInfos != null)
                {
                    boxInfos.SampleBoxEpc = com.SampleBoxEpc;
                    boxInfos.State = com.State;
                    context.BoxInfos.Update(boxInfos);
                }
                SampleBoxInfo? sampleBoxInfo = context.SampleBoxInfos.Where(p=>p.SampleBoxEpc == com.SampleBoxEpc).FirstOrDefault();
                if (sampleBoxInfo != null)
                {
                    sampleBoxInfo.SampleNum = com.SampleNum;
                    context.SampleBoxInfos.Update(sampleBoxInfo);
                }
                context.SaveChanges();
                return WebApiResponse.Create(200, "操作修改成功");
            }
            catch (Exception ex)
            {
                return WebApiResponse.Create(500, ex.Message);
            }
        }
    }
}
