﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;

namespace SwtdApi.Controllers
{
    /// <summary>
    /// 放样/取样修改箱门和样品箱
    /// </summary>
    [Controller]
    [Route("Common")]
    [LoginFilter]
    public class CommonController
    {
        [HttpPost("EditBoxAndSampleBox")]
        public WebApiResponse EditBoxAndSample([FromBody] Comm com)
        {
            return CommonManage.EditBoxAndSample(com);
        }
    }



    public class Comm
    {
        public int BoxNum { get; set; }
        public string CabName { get; set; }
        public string SampleBoxEpc { get; set;}
        public string State { get; set;}
        public int SampleNum { get; set;}

    }
}
