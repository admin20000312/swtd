﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;
using SwtdApi.Models;

namespace SwtdApi.Controllers
{
    [Controller]
    [Route("Cabinet")]
    [LoginFilter]
    public class CabinetController
    {

        /// <summary>
        /// 通过全部箱柜数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("selectAll")]
        public WebApiResponse SelectAll()
        {
            return CabinetManage.SelectAll();
        }
        
        /// <summary>
        /// 新增一个箱柜
        /// </summary>
        /// <returns></returns>
        [HttpPost("AddCabinet")]
        public WebApiResponse AddCabinet(string name)
        {
            return CabinetManage.AddCabinet(name);
        }
        
        
        /// <summary>
        /// 编辑箱柜
        /// </summary>
        /// <returns></returns>
        [HttpPost("EditCabinet")]
        public WebApiResponse EditCabinet(string id,string name)
        {
            return CabinetManage.EditCabinet(id,name);
        }
        
        /// <summary>
        /// 删除一个箱柜
        /// </summary>
        /// <returns></returns>
        [HttpPost("DeleteCabinet")]
        public WebApiResponse DeleteCabinet(string id)
        {
            return CabinetManage.DeleteCabinet(id);
        }



    }
}
