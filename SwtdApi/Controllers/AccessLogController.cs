﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;
using SwtdApi.Models;

namespace SwtdApi.Controllers
{

    [Controller]
    [Route("AccessLog")]
    [LoginFilter]
    public class AccessLogController
    {
        [HttpPost("selectAll")]
        public WebApiResponse SelectAll()
        {
            return AccessLogManage.SelectAll();
        }
        
        
        [HttpPost("AddAccessLog")]
        public WebApiResponse AddAccessLog(int type,[FromBody] AccessLog al)
        {
            return AccessLogManage.AddAccessLog(type,al);
        }
    }
}
