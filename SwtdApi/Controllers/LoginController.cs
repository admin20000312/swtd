﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;

namespace SwtdApi.Controllers
{

    [Controller]
    [Route("UserLogin")]
    public class LoginController
    {
        [HttpPost("Login")]
        public WebApiResponse Login(string name,string pwd)
        { 
            return LoginManage.Login(name,pwd);
        }
    }
}
