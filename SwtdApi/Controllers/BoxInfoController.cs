﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;
using SwtdApi.Models;

namespace SwtdApi.Controllers
{
    [Controller]
    [Route("Box")]
    [LoginFilter]
    public class BoxInfoController
    {
        /// <summary>
        /// 通过全部箱柜数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("selectAll")]
        public WebApiResponse SelectAll()
        {
            return BoxManage.SelectAll();
        }
        
        /// <summary>
        /// 通过全部箱柜数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("SelectBoxByCab")]
        public WebApiResponse SelectBoxByCab(string cabName)
        {
            return BoxManage.SelectBoxByCab(cabName);
        }

        /// <summary>
        /// 初始化箱柜的箱格数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("InitBox")]
        public WebApiResponse InitBox(string cabNum,int num)
        {
            return BoxManage.InitBox(cabNum,num);
        }
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("EditBox")]
        public WebApiResponse EditBox([FromBody] BoxInfo bi)
        {
            return BoxManage.EditBox(bi);
        }
        
        
        [HttpPost("DeleteBox")]
        public WebApiResponse DeleteBox(string json)
        {
            return BoxManage.DeleteBox(json);
        }
    }
}
