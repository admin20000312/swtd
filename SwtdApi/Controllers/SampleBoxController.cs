﻿using Microsoft.AspNetCore.Mvc;
using SwtdApi.Common;
using SwtdApi.Manage;
using SwtdApi.Models;

namespace SwtdApi.Controllers
{
    [Controller]
    [Route("SampleBox")]
    [LoginFilter]
    public class SampleBoxController
    {
        /// <summary>
        /// 通过全部样品箱
        /// </summary>
        /// <returns></returns>
        [HttpPost("selectAll")]
        public WebApiResponse SelectAll()
        {
            return SampleBoxManage.SelectAll();
        }

        [HttpPost("AddSampleBox")]
        public WebApiResponse AddSampleBox(string name, string epc)
        {
            return SampleBoxManage.AddSampleBox(name, epc);
        }


        [HttpPost("EditSampleBox")]
        public WebApiResponse EditSampleBox(string id, string name, string epc)
        {
            return SampleBoxManage.EditSampleBox(id, name,epc);
        }

       
        [HttpPost("DeleteSampleBox")]
        public WebApiResponse DeleteSampleBox(string id)
        {
            return SampleBoxManage.DeleteSampleBox(id);
        }
    }
}
